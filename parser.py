# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 08:07:51 2021

@author: N945017
"""

'''
Script to get a list of compass-IDs and converts the according html pages to xml
'''

# Input file -> csv format, one column with IDs, must be in the same folder as this file

import pandas as pd
import os

ids = pd.read_csv('./input.csv',  header = None)
ids.rename(columns = {ids.columns[0]: 'Compass_ids'}, inplace = True)


print("started parsing")

for idx, row in ids.iterrows():
    #id = row.Compass_ids
    os.system("python firmenbuch_to_xml.py https://daten.compass.at/WirtschaftsCompass/profile/company/" + str(row.Compass_ids))
    print("done with " + str(idx + 1) + "/" + str(len(ids)))
