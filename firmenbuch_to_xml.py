# -*- coding: utf-8 -*-
"""
Created on Tue Feb 2 15:18:59 2021

@author: N945017
"""

# execute file with 'parser.py' which gets a list of compass ids to run this code through

import requests
from bs4 import BeautifulSoup as bs
import lxml.etree as ET
from xml.dom import minidom
import numpy as np
import pandas as pd
import re
import datetime
import logging
import webbrowser
import time
from parser_functions import * # custom functions in separate .py file
import os
import sys
import io
import tempfile

# set up the log file
logging.basicConfig(filename = 'parser.log', level = logging.DEBUG)

#%%
# set up the connection
s = requests.Session()

s.proxies = {
 "http": "http://proxy.mib-is.org:8080",
 "https": "http://proxy.mib-is.org:8080",
}

# try to connect to daten.compass.at, if the connection fails, 
# the code will open the browser and try to establish the connection again
try:
    #print('try to establish a connection...')
    # log-in-url
    s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
    #print('success!')

except:
    for i in range(5):
        try:
            #print('trying again...')
            s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
            #print('success!')
            break
        except:
            #print('open the browser and try again...')
            webbrowser.open('https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass', autoraise = False)
            time.sleep(6)
        #if i == 5:
            #print("tried 5 times, didn't work...")

logging.info("Current time: %s" %datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
logging.info("---Firmenprofil---")
# Different companies to try out

# Compass-Verlag
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/42971"

# Porr AG
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/28352"

# Unicredit Bank Austria
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/3587313"
# no Rechtstatssachen in html??
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/59981"

# Voestalpine AG
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/404138"

# Kleinunternehmen
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/1558307"
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/205533774"

# A1
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/10368305"

# Auer-Blachke
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/120174386"

# Andritz
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/67410"

# Erste Bank
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/10573811"
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/103692716"

# Dyckerhoff
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/639939"

# Zeinzinger e.U.
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/75562589"

# Truck Center Süd GmbH
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/126178196"

# kleine gmbh
#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/205938544"

#url = 'https://daten.compass.at/WirtschaftsCompass/profile/company/69519985'

#url = "https://daten.compass.at/WirtschaftsCompass/profile/company/2007740"


# get the url of the company as the argument passed in the loop in parser.py
url = sys.argv[1]


r = s.get(url)

logging.info('Retrieved webpage with id %s' %re.split('/', url)[-1])

html_messy = r.text
html = html_messy.translate(str.maketrans({'\n': None, '\t': None, '\r': None}))

soup = bs(html, 'html.parser')

# from the compass-id alone, it is impossible to know whether it directs to a "firmenprofil" or a "gewerbeprofil"
# some companies have both, but some only have one

# Error -> when the ID is actually for a Gewerbeprofil
# Note: The syntax of these errors might change in the future!

#if str(soup).find('Error while saving/loading cache') != -1 or str(soup).find('Fehler beim Neugenerieren des Caches') != -1:
if str(soup).find('<i class="erroricon icon-message_warning"></i>') != -1:
    comp_id = re.split('/', url)[-1]
    logging.info("found no firmenprofil with id %s" %comp_id)
    logging.info("try gewerbeprofil")
    # execute script gewerbeprofil_to_xml which was made for extracting the gewerbeprofil of a company
    
    url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/" + comp_id
    
    os.system("python gewerbeprofil_to_xml.py " + url)
    
    # exit script, as there is no firmenprofil to extract
    sys.exit()
    



'''
Notes:
    don't use 's' as a iterator in loops -> s is needed for the html request (later in the code for retrieving balance sheets)
'''

'''
Extract tables from the html -> can be re-formatted to xml
in cases where this doesn't work (multiple entries,...) extract information from html with regex

To print the tree on the console:
    print(minidom.parseString(ET.tostring(tree, encoding = 'UTF-8')).toprettyxml())
    
relevanz = 99: self-made elements -> additional information that does not occur in the original compass output like the link to the page, or
the volltext of an adress
'''


# Store all links to subpages on daten.compass.at within the html page in a table and try to retrieve the pages

# search for all the urls for subpages which will be needed later
all_urls = re.findall('href="/WirtschaftsCompass/profile/(.+?)"', str(soup))
logging.info('saved all urls')

#%%
pages = []
success = []

# try to retrieve the according urls
for u in all_urls:
    try:
        subpage_messy = s.get('https://daten.compass.at/WirtschaftsCompass/profile/' + u).text
        subpage = subpage_messy.translate(str.maketrans({'\n': None, '\t': None, '\r': None}))
        soup_subpage = bs(subpage, 'html.parser')
        
        # append to list of pages and record if the attempt was succesful
        pages.append(str(soup_subpage))
        success.append('yes')
    except:
        # if the attempt was not succesful, append an empty line and go through the unsuccesful attempts later again
        pages.append(None)
        success.append('no')

# create a df out of the page strings and success information
pages_df = pd.DataFrame(list(zip(all_urls, pages, success)), columns = ['urls', 'page', 'success'])
#%%  

# if some trials were unsuccesful, try again       
for i in range(5):
    if np.isin(pages_df.success, 'no').any():
        webbrowser.open('https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass', autoraise = False)
        time.sleep(6)
        s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
        
        for idx, row in pages_df[pages_df.success == 'no'].iterrows():
    
            try:
                subpage_messy = s.get('https://daten.compass.at/WirtschaftsCompass/profile/' + all_urls[idx]).text
                subpage = subpage_messy.translate(str.maketrans({'\n': None, '\t': None, '\r': None}))
                soup_subpage = bs(subpage, 'html.parser')
                
                pages_df.iloc[idx].page = str(soup_subpage)
                pages_df.iloc[idx].success = 'yes'
                
            except:
                next
    
    else:
        break

# create a temporary directory for the subpages which will be deleted at the end of the code

temp = tempfile.TemporaryDirectory()

# temporary directory created in a path similar to 'C:\\Users\\N945017\\AppData\\Local\\Temp\\tmpvuh33re6'

logging.info('created temporary directory at %s' %temp)

# store webpages as files in temporary directory
for idx, row in pages_df.iterrows():
    with io.open(temp.name + '\\' + re.sub('/', '_', row.urls) + '.txt', 'w', encoding = 'utf-8') as text_file:
        text_file.write(row.page)        

# delete dataframe to save space
del pages_df

#%%
# define Bundesländer, will be used later, orderd so that the attribute 'art' can be matched to the index position of the bundesland in this list
bundeslaender = ['Österreich', 'Wien', 'Niederösterreich', 'Oberösterreich', 'Steiermark', 'Salzburg', 'Kärnten', 'Tirol', 'Vorarlberg', 'Burgenland']
# extract table 'firmenprofil-kommunikation' from html, which contains basic contact information for each company
firmenprofil_kommunikation = str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'}))
#%%
# delete segment about Niederlassungen -> gets handled later (otherwise would lead to problems regarding the structure of the xml)
# basically split the html string into two sections, before the segment about Niederlassungen and after the segment,
#  then paste together the two parts to get the string without the segment

# search for headers in the table
headers = re.findall('<tr><th>(.+?)</th>', firmenprofil_kommunikation)

# search for the position of 'Niederlassungen', if it occurs in the website
if [headers.index(he) for he in headers if 'Niederlassungen' in he] == []:
    pass

else:
    ind_niederlassungen = [headers.index(he) for he in headers if 'Niederlassungen' in he][0]
    
    before_niederlassungen = re.findall('(.+?)<tr><th>Niederlassungen', firmenprofil_kommunikation)[0]
    #print(before_niederlassungen)
    # if niederlassungen is the last element in the table firmenprofil_kommunikation
    if ind_niederlassungen == (len(headers) - 1):
        after_niederlassungen = ''    
    else:
        header_after_niederlassungen = headers[ind_niederlassungen + 1]
        after_niederlassungen = re.findall(header_after_niederlassungen + '(.+)', firmenprofil_kommunikation)[0]
    #print(after_niederlassungen)
    firmenprofil_kommunikation = before_niederlassungen + after_niederlassungen
#%%
# use pandas function to create a dataframe out of the html table
firmenprofil_kommunikation = pd.read_html(firmenprofil_kommunikation)[0]

# if multiple sub-companies exist, there occurs a problem with multiindices which is solved with this try / except block
try:
    firmenprofil_kommunikation.columns = firmenprofil_kommunikation.columns.droplevel()
except:    
    pass

# rename the columns
firmenprofil_kommunikation.rename(columns = {firmenprofil_kommunikation.columns[0]: 'Name', firmenprofil_kommunikation.columns[1]: 'Value'}, inplace = True)

# apply custom table_to_xml function to our table and turn into string
firmenprofil_kommunikation_string = '\n'.join(firmenprofil_kommunikation.apply(table_to_xml, axis=1))
# add 'root' node for xml
firmenprofil_kommunikation_string = '<firma>\n' + firmenprofil_kommunikation_string + '\n</firma>'
# the & sign leads to errors, so replace it
firmenprofil_kommunikation_string = re.sub('&', '&amp;', firmenprofil_kommunikation_string)

# create xml
tree = ET.ElementTree(ET.fromstring(re.sub('\n', '', firmenprofil_kommunikation_string)))

firma = tree.getroot()

# set attribute
firma.set("relevanz", "1")
logging.info('created basic tree')

# rename elements which are captured correctly but with the wrong tag name
for element in firma:
    # 'Firmenwortlaut' on the website was changed to 'Name lt. Firmenbuch' in 04/2021
    #if element.tag == 'firmenwortlaut':
    #    element.tag = 'name'
    if element.tag == 'name_lt__firmenbuch':
        element.tag = 'name'
    if element.tag == 'adresse':
        element.tag = 'sitzadresse'
    # postanschrift = betriebsadresse?
    if element.tag == 'postanschrift':
        element.tag = 'betriebsadresse'
    if element.tag == 'historischer_firmenwortlaut':
        element.tag = 'firmennamealt'
    if element.tag == 'historische_adresse':
        element.tag = 'sitzadressenalt'

firma.find('name').set("relevanz", "1")
# starting in 04/2021 the string 'Im Monitoring Ihrer Unternehmensgruppe' is displayed on the website and mistakenly attached to the name of the company
firma.find('name').text = re.sub('Im Monitoring Ihrer Unternehmensgruppe', '', firma.find('name').text)


# create Compass ID from url
create_SubElement(firma, 'id', attrib = {"relevanz" : "2"}, _text = re.split('[/]', url)[-1])
create_SubElement(firma, 'link', attrib = {"relevanz" : "99"}, _text = url) 

# see if company still exists -> text 'Firma gelöscht' etc.
# Note: other variants might be possible! These were the ones I found
special = ['Firma gelöscht', 'Konkursverfahren', 'Sanierungsverfahren aufgehoben', 'Konkurseröffnung']

# check if any of these phrases occur on the webpage    
if all([str(soup).find(x) == -1 for x in special]):
    create_SubElement(firma, 'status', attrib = {"relevanz" : "1"}, _text = 'aufrecht')
    
else:
    bool_special = [str(soup).find(x) != -1 for x in special]
    status = special[np.where(bool_special)[0][0]]
    create_SubElement(firma, 'status', attrib = {"relevanz" : "1"}, _text = status)
    
# create node 'kontakt' -> will contain information such as telephone numbers
create_SubElement(firma, 'kontakt')

# sitzadresse -> work with regex to extract full adress from string
if firma.find('sitzadresse') is None:
    create_SubElement(firma, 'sitzadrese', attrib = {"relevanz" : "1"})

else:
    sitzadresse = firma.find('sitzadresse').text
    
    # delete original text from tree
    firma.find('sitzadresse').text = ''
    firma.find('sitzadresse').set("relevanz", "1")
    
    # use custom function to determine the elements of the adress and attach them to the tree
    adress_clean = company_adress(sitzadresse)
    create_SubElement(firma.find('sitzadresse'), 'plz', _text = adress_clean.get('plz'))
    create_SubElement(firma.find('sitzadresse'), 'ort', _text = adress_clean.get('ort'))
    create_SubElement(firma.find('sitzadresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
    create_SubElement(firma.find('sitzadresse'), 'land', _text = adress_clean.get('land'))
    create_SubElement(firma.find('sitzadresse'), 'strasse', _text = adress_clean.get('strasse'))
    create_SubElement(firma.find('sitzadresse'), 'volltext', _text = sitzadresse, attrib = {"relevanz" : "99"})
    
#%%
# same for betriebsadresse
if firma.find('betriebsadresse') is None:
    create_SubElement(firma, 'betriebsadresse', attrib = {"relevanz" : "2"}) # occurs in every output

else:
    betriebsadresse = firma.find('betriebsadresse').text

    adress_clean = company_adress(betriebsadresse)
    create_SubElement(firma.find('betriebsadresse'), 'plz', _text = adress_clean.get('plz'))    
    create_SubElement(firma.find('betriebsadresse'), 'ort', _text = adress_clean.get('ort'))
    create_SubElement(firma.find('betriebsadresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
    create_SubElement(firma.find('betriebsadresse'), 'land', _text = adress_clean.get('land'))
    create_SubElement(firma.find('betriebsadresse'), 'strasse', _text = adress_clean.get('strasse'))
    create_SubElement(firma.find('betriebsadresse'), 'volltext', _text = betriebsadresse, attrib = {"relevanz" : "99"})
    firma.find('betriebsadresse').text = ''    

# remove weitere_informationen -> unnecessary
if firma.find('weitere_informationen') is None:
    pass

else:
    firma.remove(firma.find('weitere_informationen'))

# firmennamealt -> are pasted together in one cell in the df, with no clear separator, so search in the original html
# should occur in the xml output, even if it's empty
if firma.find('firmennamealt') is None:
    create_SubElement(firma, 'firmennamealt', attrib = {"relevanz" : "1"})
    logging.info('found no historische firmennamen...')

else:
    # either 'Historischer Firmenwortlaut' is the last row in the table or not
    try:
        hist_firmennamen = re.findall('<span>Historischer Firmenwortlaut.+<span>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]
    except:
        hist_firmennamen = re.findall('<span>Historischer Firmenwortlaut.+</tbody>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]
    
    # split to get the different entries in a list
    hist_firmennamen = re.split('<li>', hist_firmennamen)[1:]
    
    logging.info('found %s historische firmennamen' %len(hist_firmennamen))
    
    # create a sub-element for every entry
    # Note: Dates (from, to) cannot be found in the html, but are in the compass output
    for n in hist_firmennamen:
        # remove everything thats between < and >, we only want the text after that
        n = re.sub('<.+?>', '', n)
        create_SubElement(firma.find('firmennamealt'), 'eintrag', _text = n)
        
    firma.find('firmennamealt').text = ''
    firma.find('firmennamealt').set("relevanz", "1")
    
    logging.info('finished historische firmennamen')
    
# sitzadressenalt -> same as firmennamealt, if there's multiple entries extract from html
if firma.find('sitzadressenalt') is None:
    create_SubElement(firma, 'sitzadressenalt', attrib = {"relevanz" : "2"}) # occurs in every output
    logging.info('found no historische sitzadressen...')

else:
    try:
        hist_sitzadressen = re.findall('<span>Historische Adresse.+<span>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]
    except:
        hist_sitzadressen = re.findall('<span>Historische Adresse.+</tbody>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]

    hist_sitzadressen = re.split('<li>', hist_sitzadressen)[1:]
    
    logging.info('found %s historische sitzadressen' % len(hist_sitzadressen))
    firma.find('sitzadressenalt').set("relevanz", "1")

    for a in hist_sitzadressen:
        ind_a = hist_sitzadressen.index(a)
        a = re.sub('<.+?>', '', a)
        
        adress_clean = company_adress(a)
        
        create_SubElement(firma.find('sitzadressenalt'), 'sitzadresse')
    
        create_SubElement(firma.find('sitzadressenalt').findall('sitzadresse')[ind_a], 'plz', _text = adress_clean.get('plz'))
        create_SubElement(firma.find('sitzadressenalt').findall('sitzadresse')[ind_a], 'ort', _text = adress_clean.get('ort'))
        create_SubElement(firma.find('sitzadressenalt').findall('sitzadresse')[ind_a], 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
        create_SubElement(firma.find('sitzadressenalt').findall('sitzadresse')[ind_a], 'land', _text = adress_clean.get('land'))
        create_SubElement(firma.find('sitzadressenalt').findall('sitzadresse')[ind_a], 'strasse', _text = adress_clean.get('strasse'))
        create_SubElement(firma.find('sitzadressenalt').findall('sitzadresse')[ind_a], 'volltext', _text = a, attrib = {"relevanz" : "99"})

    firma.find('sitzadressenalt').text = '' 
    logging.info('finished historische sitzadressen')


#%%
# kontakt
#  telefon
if firma.find('telefon') is None:
    logging.info('found no phone number...')

else:
    create_SubElement(firma.find('kontakt'), 'telefonnummern', attrib = {"relevanz" : "2"})
    create_SubElement(firma.find('kontakt').find('telefonnummern'), 'telefon')
    create_SubElement(firma.find('kontakt').find('telefonnummern').find('telefon'), 'vorwahl', _text = re.split(' ', firma.find('telefon').text)[0])
    create_SubElement(firma.find('kontakt').find('telefonnummern').find('telefon'), 'nummer', _text = ' '.join(re.split(' ', firma.find('telefon').text)[1:]).lstrip())
    
    # remove original element -> wrong level
    firma.remove(firma.find('telefon'))

#  fax
if firma.find('fax') is None:
    logging.info('found no fax number...')

else:
    create_SubElement(firma.find('kontakt'), 'faxnummern', attrib = {"relevanz" : "2"})
    create_SubElement(firma.find('kontakt').find('faxnummern'), 'fax')
    create_SubElement(firma.find('kontakt').find('faxnummern').find('fax'), 'vorwahl', _text = firma.find('fax').text[:2])
    create_SubElement(firma.find('kontakt').find('faxnummern').find('fax'), 'nummer', _text = firma.find('fax').text[2:].lstrip())
    firma.remove(firma.find('fax'))


#  websites: if theres more than one, they are all pasted together in one cell so split them and turn into a list
if firma.find('internet_adresse') is None:
    logging.info('found no internet adresses...')

else:
    create_SubElement(firma.find('kontakt'), 'homepages')

    # either 'Internet-Adresse' is the last row in the table or not
    try:
        websites = re.findall('<span>Internet-Adresse.+?<span>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]
    except:
        websites = re.findall('<span>Internet-Adresse.+</tbody>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]
    
    # split to get the different entries in a list
    websites = re.split('<div>', websites)[1:]
    
    logging.info('found %s websites' %len(websites))
    
    # create a sub-element for every entry
    for ws in websites:
        # remove everything thats between < and >, we only want the text after that
        ws = re.sub('<.+?>', '', ws)
        create_SubElement(firma.find('kontakt').find('homepages'), 'www', _text = ws, attrib = {"relevanz" : "2"})

    firma.remove(firma.find('internet_adresse'))   
     

#  mailadressen
if firma.find('e_mail') is None:
    pass

else:
    create_SubElement(firma.find('kontakt'), 'emailadressen', attrib = {"relevanz" : "2"})
    create_SubElement(firma.find('kontakt').find('emailadressen'), 'email', _text = firma.find('e_mail').text)
    firma.remove(firma.find('e_mail'))

# ersteintrag
if str(soup).find('<span>Ersteintragung</span>') == -1:
    pass

else:
    create_SubElement(firma, 'ersteintrag', _text = 
                      datetime.datetime.strptime(re.sub('<.+?>', '', re.findall('<span>Ersteintragung(.+?)</td>', str(soup))[0]), '%d.%m.%Y').strftime('%Y-%m-%d'))

# letzteintrag
if str(soup).find('<span>Letzte Eintragung</span>') == -1:
    pass

else:
    create_SubElement(firma, 'letzteintrag', _text = 
                      datetime.datetime.strptime(re.sub('<.+?>', '', re.findall('<span>Letzte Eintragung(.+?)</td>', str(soup))[0]), '%d.%m.%Y').strftime('%Y-%m-%d'))


#%%
#  korrespondenz -> seperated by ','
if firma.find('korrespondenz') is None:
    pass

else:
    firma.find('korrespondenz').set("relevanz", "2")
    languages = re.split(',', firmenprofil_kommunikation.loc[firmenprofil_kommunikation.Name == 'Korrespondenz'].Value.values[0])

    for l in languages:
        create_SubElement(firma.find('korrespondenz'), 'eintrag', _text = l)
    firma.find('korrespondenz').text = ''
 
#######################################################################################################################
# extract table 'firmenprofil-produkte-dienstleistungen'
# -> not in every entry

try:
    firmenprofil_produkte_dienstleistungen = pd.read_html(str(soup.find('table', attrs={'id': 'firmenprofil-produkte-dienstleistungen'})))[0]
    logging.info('found table produkte-dienstleistungen')
except:
    pass

# if multiple sub-companies exist, there occurs a problem with multiindices which is solved with this try / except block
try:
    firmenprofil_produkte_dienstleistungen.columns = firmenprofil_produkte_dienstleistungen.columns.droplevel()
except:    
    pass

try:
    # rename the columns
    firmenprofil_produkte_dienstleistungen.rename(columns = {firmenprofil_produkte_dienstleistungen.columns[0]: 'Name', firmenprofil_produkte_dienstleistungen.columns[1]: 'Value'}, inplace = True)

    # extract from html
    #suchworte = re.findall('<span>Produkte.+<span class', str(soup.find('table', attrs={'id': 'firmenprofil-produkte-dienstleistungen'})))[0]
    if firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'Produkte'].empty:
        pass

    else:
        suchworte = re.findall('<span>Produkte.+?</ul>', str(soup.find('table', attrs={'id': 'firmenprofil-produkte-dienstleistungen'})))[0]
    
        suchworte = re.split('<li>', suchworte)[1:]
    
        suchworte_neu = []
    
        for w in suchworte[:-1]:
            w = re.sub('<.+?>', '', w)
            suchworte_neu.append(w)
    
        suchworte_neu.append(str(re.sub('<span>', '', re.split('</span>', suchworte[-1])[0])))
    
        create_SubElement(firma, 'suchworte', attrib = {"relevanz" : "2"})
    
        for w in suchworte_neu:
            ind_w = suchworte_neu.index(w)
            create_SubElement(firma.find('suchworte'), 'suchwort')
            create_SubElement(firma.find('suchworte').findall('suchwort')[ind_w], 'text', _text = w)
            #create_SubElement(firma.find('suchworte'), 'suchwort', _text = w)
            
    # geschaeftszweig
    if firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'Geschäftszweig'].empty:
        pass
    
    else:
        create_SubElement(firma, 'geschaeftszweig', _text = firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'Geschäftszweig'].Value.values[0], attrib = {"relevanz" : "1"})
    
    
    # oenace
    if firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'OENACE 2008'].empty:
        pass
    
    else:
        create_SubElement(firma, 'oenace', attrib = {"relevanz" : "2"})
    
        oenace_klassifizierungen_raw = firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'OENACE 2008'].Value.values
    
        oenace_klassifizierungen_all = re.findall(':(.+?)\\(', oenace_klassifizierungen_raw[0])
        oenace_klassifizierungen_werte = re.findall('[0-9]+\\.[0-9]+-[0-9]', oenace_klassifizierungen_raw[0])
    
        for k in oenace_klassifizierungen_all: 
        
            create_SubElement(create_SubElement(firma.find('oenace'), 'klassifizierung'), 'text', _text = k.rstrip())
            ind = oenace_klassifizierungen_all.index(k)
            create_SubElement(firma.find('oenace').findall('klassifizierung')[ind], 'wert', _text = oenace_klassifizierungen_werte[ind])
    
    
    # Hauptbranche
        create_SubElement(firma, 'hauptbranche', attrib = {"relevanz" : "2"})
        create_SubElement(firma.find('hauptbranche'), 'klassifizierung')
        create_SubElement(firma.find('hauptbranche').find('klassifizierung'), 'text',  _text = re.findall('Hauptbranche:(.+?)\\(', oenace_klassifizierungen_raw[0])[0].rstrip())
        create_SubElement(firma.find('hauptbranche').find('klassifizierung'), 'wert',  _text = re.findall('[0-9]+\\.[0-9]+-[0-9]', oenace_klassifizierungen_raw[0])[0].rstrip())
        logging.info('finished oenace classifications')
    
    # taetigkeit
    if firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'Tätigkeiten'].empty:
        pass
    
    else:
        create_SubElement(firma, 'taetigkeit', _text = firmenprofil_produkte_dienstleistungen.loc[firmenprofil_produkte_dienstleistungen.Name == 'Tätigkeiten'].Value.values[0], attrib = {"relevanz" : "2"})

except:
    pass    

# Gewerbedaten -> how to find out bundesländer?
create_SubElement(firma, 'gewerbedaten', attrib = {"relevanz" : "3"})

# gewerbe id
try:
    create_SubElement(firma.find('gewerbedaten'), 'id', _text = re.findall('/tradeperson/([0-9]+)', str(soup))[0], attrib = {"relevanz" : "3"})

except:
    pass



#%%
#######################################################################################################################
# extract table 'firmenprofil-stammdaten'
try:
    firmenprofil_stammdaten = pd.read_html(str(soup.find('table', attrs={'id': 'firmenprofil-stammdaten'})))[0]
    logging.info('found table stammdaten')
except:
    pass



# if multiple sub-companies exist, there occurs a problem with multiindices which is solved with this try / except block
try:
    firmenprofil_stammdaten.columns = firmenprofil_stammdaten.columns.droplevel()
except:    
    pass

try:
    # rename the columns
    firmenprofil_stammdaten.rename(columns = {firmenprofil_stammdaten.columns[0]: 'Name', firmenprofil_stammdaten.columns[1]: 'Value'}, inplace = True)
    
    firmenprofil_stammdaten_string = '\n'.join(firmenprofil_stammdaten.apply(table_to_xml, axis=1))
    firmenprofil_stammdaten_string = '<firma>\n' + firmenprofil_stammdaten_string + '\n</firma>'
    firmenprofil_stammdaten_string = re.sub('&', '&amp;', firmenprofil_stammdaten_string)
    firmenprofil_stammdaten_string = re.sub('kapital/gründungsprivilegierung', 'kapital_gründungspriveligierung', firmenprofil_stammdaten_string)
    
    stammdaten_tree = ET.ElementTree(ET.fromstring(re.sub('\n', '', firmenprofil_stammdaten_string)))
    
    stammdaten_tree_root = stammdaten_tree.getroot()
    
    # append to large tree
    for el in stammdaten_tree_root:
        firma.append(el)
    
    logging.info('appended table stammdaten to tree')
    
except:
    pass 



# rechtsform works
if firma.find('rechtsform') is None:
    pass
else:
    firma.find('rechtsform').set("relevanz", "1")
# sitz
if firma.find('sitz_in') is None:
    pass

else:
    firma.find('sitz_in').tag = 'sitz'
    firma.find('sitz').set("relevanz", "1")

# gruendung
if firma.find('gründungsjahr__besteht_seit_') is None:
    pass
else:
    create_SubElement(firma, 'gruendung', _text = firma.find('gründungsjahr__besteht_seit_').text, attrib = {"relevanz" : "2"})
    firma.remove(firma.find('gründungsjahr__besteht_seit_'))

# hfu-verzeichnis -> ??? not in the html...

# bankverbindung
# multiple entries -> extract from html
if firma.find('bankverbindung') is None:
    pass

else:
    # either 'Bankverbindung' is the last row in the table or not
    try:
        bankverbindungen = re.findall('<span>Bankverbindung.+<span>', str(soup.find('table', attrs={'id': 'firmenprofil-stammdaten'})))[0]
    except:
        bankverbindungen = re.findall('<span>Bankverbindung.+</tbody>', str(soup.find('table', attrs={'id': 'firmenprofil-stammdaten'})))[0]
    
    #bankverbindungen = re.findall('<span>Bankverbindung.+?</tbody>', str(soup))[0]
    
    bankverbindungen = re.split('<li>', bankverbindungen)[1:]

    for b in bankverbindungen[:-1]: # check if the last element is always empty??
        b = re.sub('<.+?>', '', b)
        # the last entry contains the string 'Compass-ID', which is unnecessary
        b = re.sub('Compass-ID', '', b)
        create_SubElement(firma.find('bankverbindung'), 'eintrag', _text = b)
        
    firma.find('bankverbindung').text = ''
    firma.find('bankverbindung').set("relevanz", "2")

# lei -> what is this? -> cannot be found in the xml but here on the website
if firma.find('lei') is None:
    pass
else:
    firma.remove(firma.find('lei'))

# bankleitzahlen -> same name as in the html table, so works automatically
if firma.find('bankleitzahlen') is None:
    pass
else:
    firma.find('bankleitzahlen').set("relevanz", "9")

####################################################################################################################################
# extract table 'firmenprofil-wirtschaftsdaten'
# -> not in every entry

try:
    firmenprofil_wirtschaftsdaten = pd.read_html(str(soup.find('table', attrs={'id': 'firmenprofil-wirtschaftsdaten'})))[0]
    logging.info('found table wirtschaftsdaten')
except:
    pass

# if multiple sub-companies exist, there occurs a problem with multiindices which is solved with this try / except block
try:
    firmenprofil_wirtschaftsdaten.columns = firmenprofil_wirtschaftsdaten.columns.droplevel()
except:    
    pass

try:
    # rename the columns
    firmenprofil_wirtschaftsdaten.rename(columns = {firmenprofil_wirtschaftsdaten.columns[0]: 'Name', firmenprofil_wirtschaftsdaten.columns[1]: 'Value'}, inplace = True)

    firmenprofil_wirtschaftsdaten_string = '\n'.join(firmenprofil_wirtschaftsdaten.apply(table_to_xml, axis=1))
    firmenprofil_wirtschaftsdaten_string = '<firma>\n' + firmenprofil_wirtschaftsdaten_string + '\n</firma>'
    firmenprofil_wirtschaftsdaten_string = re.sub('&', '&amp;', firmenprofil_wirtschaftsdaten_string)

    wirtschaftsdaten_tree = ET.ElementTree(ET.fromstring(re.sub('\n', '', firmenprofil_wirtschaftsdaten_string)))

    wirtschaftsdaten_tree_root = wirtschaftsdaten_tree.getroot()
    
    # append to large tree
    for el in wirtschaftsdaten_tree_root:
        firma.append(el)
    logging.info('appended table wirtschaftsdaten to tree')
    
except:
    pass    
#%%
# delete unnecessary entries:
if firma.find('wirtschaftliche_entwicklung') is None:
    pass

else:
    firma.remove(firma.find('wirtschaftliche_entwicklung'))


#%%
# umsaetze, bilanzsumme, egts, cashflows
for e in ['umsätze', 'bilanzsumme', 'evs__egt_', 'cashflow']:
    if firma.find(e) is None:
        logging.info('found no %s...' %e)
    
    else:  
        # 'EUR' marks the end of an entry -> hopefully standardized
        eintraege = re.split('EUR', firma.find(e).text)
        
        # generate new e, as Umlaute are not beneficial in xml tags
        if e == 'umsätze':
            e_n = 'umsaetze'
            sub_tag = 'umsatz'
        elif e == 'evs__egt_':
            e_n = 'egts'
            sub_tag = 'summe'
        elif e == 'cashflow':
            e_n = 'cashflows'
            sub_tag = 'summe'
        else:
            e_n = e
            sub_tag = 'summe'
        
        create_SubElement(firma, e_n, attrib = {"relevanz" : "2"})
        
        for u in eintraege[:-1]:
            create_SubElement(firma.find(e_n), 'volumen')
            
            ind_u = eintraege.index(u)
                    
            try:
                jahr = re.findall('[0-9]{4}\\/[0-9]{2}', u)[0]
            except:
                #jahr = re.findall('[0-9]{4}', u)[0]
                jahr = re.findall('([0-9]{4}).*:', u)[0]
            
    
            # other possibilities?
            try:
                art = re.findall('\\((Gruppe)\\)', u)[0]
            except:
                art = ''
    
            create_SubElement(firma.find(e_n).findall('volumen')[ind_u], 'jahr', attrib = {'art': art}, _text = jahr)
            
            wert = re.split(':', u)[1:][0].rstrip().lstrip()
            
            if re.findall('Mio.', wert):
                wert = re.sub('Mio.', '', wert).strip()
                
                # attention!!
                # remove tausenderpunkt
                wert = re.sub('\\.', '', wert)
                #print(wert)
                # replace german comma with english comma
                wert = re.sub(',', '.', wert)
                wert = float(wert)
                wert = wert * 1000000
                #print(wert)
            
            create_SubElement(firma.find(e_n).findall('volumen')[ind_u], sub_tag, _text = str(int(wert)))
        
        if e == 'umsätze' or e == 'evs__egt_' or e == 'cashflow':
            firma.remove(firma.find(e))
        
        else:
            firma.find(e).text = ''
            
        logging.info('finished %s' %e_n)

# praemieneinnahmen -> ???
create_SubElement(firma, 'praemieneinnahmen')

# revision -> ???
create_SubElement(firma, 'revision')
#%%
# beschaeftigte -> cannot be easily split

if firma.find('beschäftigte') is None:
    logging.info('found no beschaeftigte...')

else:    
    #try:
    #    beschaeftigte = re.findall('<span>Beschäftigte.+?<span>', str(soup))[0]
    #except:
    #    beschaeftigte = re.findall('<span>Beschäftigte.+?</tbody>', str(soup))[0]
    
    #beschaeftigte = re.findall('<span>Beschäftigte.+?</tbody>', str(soup))[0]
    beschaeftigte = re.findall('<span>Beschäftigte.+?</td>', str(soup))[0]
    
    beschaeftigte = re.split('<li>', beschaeftigte)[1:] 
    
    firma.remove(firma.find('beschäftigte'))

    create_SubElement(firma, 'beschaeftigte', attrib = {"relevanz" : "2"})

    for b in beschaeftigte:
        ind_b = beschaeftigte.index(b)
        create_SubElement(firma.find('beschaeftigte'), 'mitarbeiter')

        b = re.sub('<.+?>', '', b)
        
        # often in the last element the string 'Wirtschaftliche Entwicklung' is attached -> delete
        b = re.sub('Wirtschaftliche Entwicklung', '', b)
        
        # sometimes the last element of beschaeftigte is empty
        if b == '':
            continue
                
        try:
            jahr = re.findall('([0-9]{4}).*:', b)[0]
        except:
            jahr = re.findall('[0-9]{4}\\/[0-9]{2}', b)[0]

        # other possibilities?
        try:
            art = re.findall('\\((Gruppe)\\)', b)[0]
        except:
            art = ''   
           
        gesamt = re.split(':', b)[1:][0].rstrip().lstrip()    

        create_SubElement(firma.find('beschaeftigte').findall('mitarbeiter')[ind_b], 'jahr', attrib = {'art': art}, _text = jahr)
                
        create_SubElement(firma.find('beschaeftigte').findall('mitarbeiter')[ind_b], 'gesamt', _text = gesamt)
    logging.info('finished beschaeftigte')

#%%
# Jahresabschluss, Konzernabschluss
# -> numbers not on main page but on subpage
for a in ['Jahresabschluss', 'Konzernabschluss']:
    # create lowercase version of iterator
    a_l = a.lower()
    if firma.find(a_l) is None:
        logging.info('found no data on %s' %a_l)
        
    else:
        # entries either at the end of the table or somewhere within
        try:
            abschluss = re.findall('<span>' + a + '.+?<th>', str(soup.find('table', attrs={'id': 'firmenprofil-wirtschaftsdaten'})))[0]
        except:
            abschluss = re.findall('<span>' + a + '.+?</table>', str(soup.find('table', attrs={'id': 'firmenprofil-wirtschaftsdaten'})))[0]
        #abschluss = re.findall('<span>' + a + '.+?<th>', str(soup.find('table', attrs={'id': 'firmenprofil-wirtschaftsdaten'})))[0]
        eintraege = re.split('<li>', abschluss)[1:]
        
        links = []
        for l in eintraege:
            if re.search('href="/WirtschaftsCompass/profile/(.+?)"', l) is None:
                links.append(None)
            else:
                links.append(re.findall('href="/WirtschaftsCompass/profile/(.+?)"', l)[0])
        #links = re.findall('href=(.+?)target', ''.join(eintraege))
        stichtag = [re.sub('<.+?>', '', j).rstrip() for j in eintraege]
        
        for i in range(len(eintraege)):
            date = re.findall('[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}', stichtag[i])[0]
            date = datetime.datetime.strptime(date, '%d.%m.%Y').strftime('%Y-%m-%d')
            #print(date)
            
            # sometimes there is no published balance sheet
            if links[i] is None:
                stichtag_clean = re.sub('Bilanz nicht vorhanden', '', stichtag[i])
                create_SubElement(firma.find(a_l), 'eintrag', attrib={
                    'stichtag': date},
                    _text = stichtag_clean)
                
            else:
                bilanz_id = re.findall('\\/([0-9]+)', links[i])[1]
                #print(bilanz_id)
                
                with io.open(temp.name + '\\' + re.sub('/', '_', links[i]) + '.txt', 'r', encoding = 'utf-8') as file:
                    bilanzsumme = re.findall('<th class="text">Bilanzsumme</th> <th class="value">([0-9\\.\\,]+)', file.read())[0]
       
                create_SubElement(firma.find(a_l), 'eintrag', attrib={
                    'bilanzid': bilanz_id,
                    'bilanz_summe': bilanzsumme,
                    'stichtag': date},
                    _text = stichtag[i])
        
        # format date in text?
    
        firma.find(a_l).text = ''
        firma.find(a_l).set("relevanz", "1")
        logging.info('finished %s' %a_l)


        
#%%
# bilanzstichtag -> extract from jahresabschluss string
if firma.find('jahresabschluss') is None:
    pass

else:
    import locale
    # set language to german
    locale.setlocale(locale.LC_ALL, locale = 'de_DE')
    bilanzstichtag = re.findall('zum ([0-9]{2}\\.[0-9]{2}\\.)', firma.find('jahresabschluss').findall('eintrag')[0].text)[0]
    
    # a problem occurs when the bilanzstichtag is the ultimo of february (either 28 or 29), so try a year where it's not 29 February
    try:
        bilanzstichtag = datetime.datetime.strptime(bilanzstichtag, '%d.%m.').strftime('%d. %B')
    except:
        bilanzstichtag = re.findall('zum ([0-9]{2}\\.[0-9]{2}\\.)', firma.find('jahresabschluss').findall('eintrag')[1].text)[0]
        bilanzstichtag = datetime.datetime.strptime(bilanzstichtag, '%d.%m.').strftime('%d. %B')
        
    create_SubElement(firma, 'bilanzstichtag', _text = str(bilanzstichtag), attrib = {"relevanz" : "1"})

#%%
# bilanzen -> ???
create_SubElement(firma, 'bilanzen')
# kapital
if firma.find('kapital_gründungsprivilegierung') is None:
    pass

else:
    #elements = re.split('EUR', firma.find('kapital_gründungsprivilegierung').text) # always split by EUR?
    elements = re.split('[A-Z]{3}', firma.find('kapital_gründungsprivilegierung').text) # catches other currencies as well
    
    create_SubElement(firma, 'kapital', attrib = {"relevanz" : "1"})
    #create_SubElement(firma.find('kapital'), 'waehrung', _text = 'EUR')
    create_SubElement(firma.find('kapital'), 'waehrung', _text = re.findall('[A-Z]{3}', firma.find('kapital_gründungsprivilegierung').text)[0])
    create_SubElement(firma.find('kapital'), 'wert', _text = elements[1].strip())
    if len(elements) == 3:
        create_SubElement(firma.find('kapital'), 'einbezahlt', _text = re.sub('einbezahlt', '', elements[2]).strip())
    firma.remove(firma.find('kapital_gründungsprivilegierung'))

# gruendungspriveligierung -> ???
create_SubElement(firma, 'gruendungspriveligierung')
# haftung -> ???
create_SubElement(firma, 'haftung')

# fbnum
if firma.find('firmenbuchnummer') is None:
    pass

else:
    firma.find('firmenbuchnummer').tag = 'fbnum'
    # there's unnecesary text after the firmenbuchnummer -> delete
    firma.find('fbnum').text = re.split(' ', firma.find('fbnum').text)[0]
    firma.find('fbnum').set("relevanz", "1")
    
# gericht
if firma.find('gericht') is None:
    pass

else:
    # text is in the form 'Handelsgericht WienKontaktdaten ...' -> only keep first part
    firma.find('gericht').text = re.split('Kontaktdaten', firma.find('gericht').text)[0]
    firma.find('gericht').set("relevanz", "1")

# dvr
if firma.find('dvr_nummer') is None:
    pass

else:
    firma.find('dvr_nummer').tag = 'dvr'
    firma.find('dvr').set("relevanz", "2")
    
# uid
if firma.find('uid') is None:
    pass

else:
    firma.find('uid').text = re.findall('ATU[0-9]+', firma.find('uid').text)[0]
    
# oenbidentnummer
if firma.find('oenb_identnummer') is None:
    pass

else:
    firma.find('oenb_identnummer').tag = 'oenbidentnummer'
    firma.find('oenbidentnummer').set("relevanz", "2")
    

# aralizenz -> ???
create_SubElement(firma, 'aralizenz')
# furhpark -> ???
create_SubElement(firma, 'fuhrpark')
# wirtschaftsjahr -> ???
create_SubElement(firma, 'wirtschaftsjahr')
#%%



# marken -> cannot be found in html!!
create_SubElement(firma, 'marken')
# comment on website -> <!-- wird via Javascript aktualisiert -->


# import -> ???
create_SubElement(firma, 'import')
# export -> ???
create_SubElement(firma, 'export')
# exportquote -> ???
create_SubElement(firma, 'exportquote')
#%%
# niederlassungen
# 3 (or more?) types: inland, ausland, registriert
# leave out contact details for the niederlassungen?!
num_inland = 0
num_ausland = 0
num_protokolliert = 0
num_gesamt = 0

for e in ['betriebsstaetten-inland', 'betriebsstaetten-registriert', 'betriebsstaetten-ausland']:
    if str(soup).find(e) == -1:
        logging.info('found no %s...' %e)
    
    else:
        if firma.find('niederlassungen') == None:
            create_SubElement(firma, 'niederlassungen')
        else:
            pass
        
        # one entry looks like this:
            #9 protokollierte Betriebsstätte(n)</h3><div class="table-color-change" data-toggle-content="" 
            #id="betriebsstaetten-registriert"><table class="inline-table"><tbody><tr><th class="text-transform-normal">
            #<b><span>Zweigniederlassung</span></b></th><td>001</td></tr><tr><th class="text-transform-normal"><b>
            #<span>Name</span></b></th><td><b><span>PORR AG Zweigniederlassung Burgenland</span></b></td></tr><tr>
            #<th class="text-transform-normal not-bold">Adresse</th><td>Neudorferstraße-Betriebsgebiet 1<br/>
            #7111 Parndorf, Burgenland</td></tr><tr><th class="text-transform-normal not-bold">Telefon</th>
            #<td> 05 06 26-3777</td></tr><tr><th class="text-transform-normal not-bold"><span>OeNB Identnummer</span>
            #<i class="wico-info-tooltip"><tooltip text="Die OeNB Identnummer ist ein Identifikator, der von der Österreichischen 
            #Nationalbank (OeNB) vergeben wird.&lt;br/&gt;Quelle: Firmenbuch der Republik Österreich"></tooltip></i></th><td>809098</td></tr>

        niederlassungen_raw = re.findall(e + '.+?</table></div>', str(soup.find('table', attrs={'id': 'firmenprofil-kommunikation'})))[0]
        niederlassungen_list = re.split('<span>Name</span>', niederlassungen_raw)
                
        namen = {niederlassungen_list.index(nl): re.sub('<.+?>', '', re.findall('<span>.+?</span>', nl)[0]) for nl in niederlassungen_list[1:]}
        #print(namen)
        adressen = {niederlassungen_list.index(nl): re.sub('<.+?>', '', re.findall('Adresse(.+?)</tr>', nl)[0]) for nl in niederlassungen_list[1:]}
    
        logging.info('found %s %s', len(niederlassungen_list), e)
        
        sub_tag = re.split('-', e)[-1]
        if sub_tag == 'registriert':
            sub_tag = 'protokolliert'
        else:
            pass
        
        if sub_tag == 'inland':
            num_inland = len(niederlassungen_list) - 1
        
        elif sub_tag == 'ausland':
            num_ausland = len(niederlassungen_list) - 1
            
        elif sub_tag == 'protokolliert':
            num_protokolliert = len(niederlassungen_list) - 1
            
        else:
            pass
        
        df_nl = pd.DataFrame.from_dict(namen, orient = 'index')
        df_nl.rename(columns = {df_nl.columns[0]: 'Name'}, inplace = True)
        df_nl['Adress'] = df_nl.index.map(adressen)
        
        # no art for protokollierte Niederlassungen
        if sub_tag == 'protokolliert':
            pass
        
        else:    
            types = {}
            #print(separators)
            for nl in niederlassungen_list[1:]:
                ind_nl = niederlassungen_list.index(nl)
                art = re.sub('<.+?>', '', re.findall('<thead><tr><th class="text-transform-normal">(.+?)</th><th></th>', niederlassungen_raw[:niederlassungen_raw.find(nl)])[-1])
                types[ind_nl] = art
            
            
            df_nl['Types'] = df_nl.index.map(types)
            #print(df_nl)
            
        df_nl = df_nl.reset_index()
              
        create_SubElement(firma.find('niederlassungen'), sub_tag, attrib = {"relevanz" : "2"})
        
        for ind_nl, nl in df_nl.iterrows():
            #ind_nl = ind_nl - 1
            #print(nl['Adress'])
            if sub_tag == 'protokolliert':
                create_SubElement(firma.find('niederlassungen').find(sub_tag), 'niederlassung')
            
            else:
                create_SubElement(firma.find('niederlassungen').find(sub_tag), 'niederlassung', attrib={'art':nl['Types']})
            
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl], 'name', _text = nl['Name'])
            
            adress_clean = company_adress(nl['Adress'])
            
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl], 'adresse')
        
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl].find('adresse'), 'plz', _text = adress_clean.get('plz'))
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl].find('adresse'), 'ort', _text = adress_clean.get('ort'))
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl].find('adresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl].find('adresse'), 'land', _text = adress_clean.get('land'))
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl].find('adresse'), 'strasse', _text = adress_clean.get('strasse'))  
            create_SubElement(firma.find('niederlassungen').find(sub_tag).findall('niederlassung')[ind_nl].find('adresse'), 'volltext', _text = nl['Adress'], attrib = {"relevanz" : "99"})  
        
        logging.info('finished %s' %e)

create_SubElement(firma, 'zweigniederlassung-anzahl')
num_gesamt = num_inland + num_ausland + num_protokolliert
create_SubElement(firma.find('zweigniederlassung-anzahl'), 'zwnl-gesamt', _text = str(num_gesamt).zfill(4))
create_SubElement(firma.find('zweigniederlassung-anzahl'), 'zwnl-inland', _text = str(num_inland).zfill(4))
create_SubElement(firma.find('zweigniederlassung-anzahl'), 'zwnl-ausland', _text = str(num_ausland).zfill(4))
create_SubElement(firma.find('zweigniederlassung-anzahl'), 'zwnl-protokolliert', _text = str(num_protokolliert).zfill(4))

# tag niederlassungen should occur in every output
if firma.find('niederlassungen') == None:
    create_SubElement(firma, 'niederlassungen')
    create_SubElement(firma.find('niederlassungen'), 'inland')
    create_SubElement(firma.find('niederlassungen'), 'ausland')
    create_SubElement(firma.find('niederlassungen'), 'protokolliert')

#%%

################################################################################################################################
# extract table 'firmenprofil-organe-beteiligungen'
firmenprofil_organe = pd.read_html(str(soup.find('table', attrs={'id': 'firmenprofil-organe-beteiligungen'})))[0]
logging.info('found table organe')


# if multiple sub-companies exist, there occurs a problem with multiindices which is solved with this try / except block
try:
    firmenprofil_organe.columns = firmenprofil_organe.columns.droplevel()
except:    
    pass


# rename the columns
firmenprofil_organe.rename(columns = {firmenprofil_organe.columns[0]: 'Name', firmenprofil_organe.columns[1]: 'Value'}, inplace = True)

firmenprofil_organe_string = '\n'.join(firmenprofil_organe.apply(table_to_xml, axis=1))
firmenprofil_organe_string = '<firma>\n' + firmenprofil_organe_string + '\n</firma>'
firmenprofil_organe_string = re.sub('&', '&amp;', firmenprofil_organe_string)
#firmenprofil_organe_string = re.sub('[a-z]/[a-z]', '_', firmenprofil_stammdaten_string)

organe_tree = ET.ElementTree(ET.fromstring(re.sub('\n', '', firmenprofil_organe_string)))

organe_tree_root = organe_tree.getroot()

# append to large tree
for el in organe_tree_root:
    firma.append(el)

logging.info('appended table organe')

#%%
# wirtschaftliche_eigentuemer -> immer natürlicher person?
if re.findall('<span>Wirtschaftlicher Eigentümer.+?<td>.+?<td>', str(soup)) == []:
    logging.info('found no wirtschaftliche Eigentümer...')
    
else:
    create_SubElement(firma, 'wirtschaftliche_eigentuemer')
    # not for AGs, GmbHs
    # in the form of <a href="/WirtschaftsCompass/profile/naturalperson/company/3990207">Futter Heide-Lene</a>,geb. 26.08.1940</span>
   
    wirtschaftliche_eigentuemer = re.findall('<span>Wirtschaftlicher Eigentümer.+?<td>.+?<td>', str(soup))[0]
    #wirtschaftliche_eigentuemer = re.findall('<span>Wirtschaftlicher Eigentümer.+?<td>.+?<td>', str(soup.find('table', attrs={'id': 'firmenprofil-organe-beteiligungen'})))[0]

    wirtsch_eigentuemer_liste = re.findall('<a href="/WirtschaftsCompass/profile/naturalperson/.+?</span>', wirtschaftliche_eigentuemer)
    
    logging.info('found %s wirtschaftliche_eigentuemer', len(wirtsch_eigentuemer_liste))
    
    for et in wirtsch_eigentuemer_liste:
        ind_et = wirtsch_eigentuemer_liste.index(et)
        #print(et)
        #print(re.findall('<a href=.+?</span>', wirtschaftliche_eigentuemer))
        fullname = re.sub('<.+?>', '', re.split('</a>', et)[0])
        #fullname = re.split(',', re.sub('<.+?>', '', et))[0]
    
        try:
            # extract geburtsdatum from string like:
            #  <a href="/WirtschaftsCompass/profile/naturalperson/company/205533775">Harringer Moritz Andreas</a>,    geb. 25.05.1993        </span>    
            elements = re.split(' ', re.split(',', re.sub('<.+?>', '', et))[-1])
            geburt = datetime.datetime.strptime(re.sub('[a-z]', '', elements[elements.index('geb.') + 1]), '%d.%m.%Y').strftime('%Y-%m-%d')            
        except:
            #print('no date found')
            geburt = ''
              
        
        link = re.findall('<a href="/WirtschaftsCompass/profile/(.+?)"', et)[0]
        
        with io.open(temp.name + '\\' + re.sub('/', '_', link) + '.txt', 'r', encoding = 'utf-8') as file:
            parsed = file.read()
            try:
                adresse = re.findall('Adresse.*?<td>(.+?)</td>', parsed)[0]
            except:
                adresse = re.findall('Postanschrift.*?<td>(.+?)</td>', parsed)[0]
                
        adresse = re.sub('<.+?>', '', adresse)    
       # adress in the form of 'Steinbruchgasse 30-32, 2540 Gainfarn, Niederösterreich, Österreich'
                
        
        create_SubElement(firma.find('wirtschaftliche_eigentuemer'), 'eigner')
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'id', _text = re.split('/', link)[-1])
        
        # immer WIRTSCHAFTLICHER EIGENTÜMER??
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'relation', _text = 'WIRTSCHAFTLICHER EIGENTÜMER')
        #create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'geschlecht', _text = '???')
        
        
        
        
        
        # fill in fullname -> think about splitting later
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'fullname', _text = fullname, attrib = {"relevanz" : "99"})
        if re.split('/', link)[0] == 'naturalperson':
            name = name_split(fullname)
            create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'titel', _text = name.get('titel'))
            create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'vorname', _text = name.get('vorname'))
            create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'nachname', _text = name.get('nachname'))
        else:
            pass
        
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'geburt', _text = geburt)
        adress_clean = company_adress(adresse)
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'adresse')
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et].find('adresse'), 'plz', _text = adress_clean.get('plz'))
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et].find('adresse'), 'strasse', _text = adress_clean.get('strasse'))
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et].find('adresse'), 'ort', _text = adress_clean.get('ort'))
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et].find('adresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et].find('adresse'), 'land', _text = adress_clean.get('land'))
        create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et].find('adresse'), 'volltext', _text = adresse, attrib = {"relevanz" : "99"})
        #create_SubElement(firma.find('wirtschaftliche_eigentuemer').findall('eigner')[ind_et], 'vertretung', _text = '???')
    
    if firma.find('wirtschaftlicher_eigentümer__berechnet_') is None:
        pass
    else:
        firma.remove(firma.find('wirtschaftlicher_eigentümer__berechnet_'))
    
    # additional -> not always necessary
    try:
        hinweise = re.split('<div>', wirtschaftliche_eigentuemer)[-1]
        hinweise = re.sub('<.+?>', '', hinweise)
    except:
        hinweise = ''
    create_SubElement(firma.find('wirtschaftliche_eigentuemer'), 'hinweise')
    create_SubElement(firma.find('wirtschaftliche_eigentuemer').find('hinweise'), 'hinweis', _text = hinweise)
    logging.info('finished wirtschaftliche eigentuemer')



#%% 
# eigentümer, management, kontrollorgane
# these three are structures very similarly, so loop over the process
for e in ['Eigentümer', 'Management', 'Kontrollorgane']:
    # lowercase because we need that for the tag
    e_l = e.lower()
    if firma.find(e_l) is None:
        logging.info('found no %s...' %e_l)
    
    else:
        try:
            # either last entry or not
            people = re.findall('<span>' + e + '.+?<td>.+?<td>', str(soup.find('table', attrs={'id': 'firmenprofil-organe-beteiligungen'})))[0]
        except:
            people = re.findall('<span>' + e + '.+?<td>.+?</table>', str(soup.find('table', attrs={'id': 'firmenprofil-organe-beteiligungen'})))[0]
        
        #people = re.findall('<span>' + e + '.+?<td>.+?</table>', str(soup))[0]
        
        if e_l == 'eigentümer':
            e_n = 'eigentuemer'
        else:
            e_n = e_l
        
        # check if the tag already exists, if not create it
        if firma.find(e_n) == None:
            create_SubElement(firma, e_n)
        else:
            pass
        
        
        # in the form of <a href="/WirtschaftsCompass/profile/naturalperson/company/3990207">Futter Heide-Lene</a>,geb. 26.08.1940</span>
        #people_list = re.findall('<a href=.+?</li>', people)
        # not all eigentümer etc have links, so split by <li> rather than <a href>
        people_list = re.split('<li>', people)[1:]
        
        logging.info('found %s %s', len(people_list), e_l)
        
        if e_n == 'eigentuemer':
            sub_tag = 'eigner'
        else:
            sub_tag = 'person'
        
        for et in people_list:
            ind_et = people_list.index(et)
            
            # two cases: either the entry contains a link, or not
            # if its a link -> retrieve data from downloaded subpage
            if et.find('<a href') != -1:
            
                fullname = re.sub('<.+>', '', re.split('</a>', et)[0])
               
                # can be different for each person, search for last occurence of the pattern up to the point where the person is in the html
                rolle = re.sub('<.+?>', '', re.findall('<h3>(.+?)</h3>', people[:people.find(et)])[-1]).upper()
                
                try:
                    elements = re.split(' ', re.split(',', re.sub('<.+?>', '', et))[-1])
                    geburt = datetime.datetime.strptime(re.sub('[a-z]', '', elements[elements.index('geb.') + 1]), '%d.%m.%Y').strftime('%Y-%m-%d')
                except:
                    geburt = ''
                      
                
                link = re.findall('<a href="/WirtschaftsCompass/profile/(.+?)"', et)[0]
               
                with io.open(temp.name + '\\' + re.sub('/', '_', link) + '.txt', 'r', encoding = 'utf-8') as file:
                    parsed = file.read()
                    
                    try:
                        adresse = re.findall('Adresse.*?<td>(.+?)</td>', parsed)[0]
                    except:
                        adresse = re.findall('Postanschrift.*?<td>(.+?)</td>', parsed)[0]
                        
                    adresse = re.sub('<.+?>', '', adresse)
                    
                    # if it's a company, add info about company
                    if re.split('/', link)[0] == 'naturalperson':
                        pass
                    else:
                        #if np.isin(element = np.array(re.split(',', adresse)), test_elements = bundeslaender).any():
                        # if it's an Austrian company, it contains data about firmenbuchnummer etc.
                        if np.isin(element = np.array(re.split(' ', adresse)), test_elements = bundeslaender).any():
                            fbnum = re.findall('Firmenbuchnummer.*?<td>(.+?)</td>', parsed)[0]
                            rechtsform = re.findall('Rechtsform.*?<td>(.+?)</td>', parsed)[0]
                            oenbidentnummer = re.sub('<.+?>', '', re.findall('OeNB Identnummer.*?<td>(.+?)</td>', parsed)[0])
                        else:
                            fbnum = ''
                            rechtsform = ''
                            oenbidentnummer = ''
        
        
                # adress in the form of 'Steinbruchgasse 30-32, 2540 Gainfarn, Niederösterreich, Österreich'
                create_SubElement(firma.find(e_n), sub_tag)
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'id', _text = re.split('/', link)[-1])
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'relation', _text = rolle)
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'geschlecht', _text = '???')
                          
                
                # treat natural persons different than juridical persons
                if re.split('/', link)[0] == 'naturalperson':
                    name = name_split(fullname)
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'titel', _text = name.get('titel'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'vorname', _text = name.get('vorname'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'nachname', _text = name.get('nachname'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'fullname', _text = fullname, attrib = {"relevanz" : "99"})
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'geburt', _text = geburt)
                    
                    
                    # no adress in compass xml for management and kontrollorgane, so leave out here also
                    if e_n == 'management' or e_n == 'kontrollorgane':
                        pass
                    
                    else:
                        adress_clean = company_adress(adresse)
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'adresse')
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('adresse'), 'plz', _text = adress_clean.get('plz'))
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('adresse'), 'strasse', _text = adress_clean.get('strasse'))
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('adresse'), 'ort', _text = adress_clean.get('ort'))
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('adresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('adresse'), 'land', _text = adress_clean.get('land'))
                        create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('adresse'), 'volltext', _text = adresse, attrib = {"relevanz" : "99"})
                        
                
                else:
                    # quote appears only if the company is Gesellschafter, shareholder or aktionär -> other structures with quotes?
                    if np.isin(element = rolle, test_elements = ['GESELLSCHAFTER', 'SHAREHOLDER', 'AKTIONÄR']).any():
                        quote = re.findall('\\(Anteil: (.+?)\\)', et)[-1]
            
                    else:
                        quote = ''
                        
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'fbnum', _text = fbnum)
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'name', _text = fullname)
                    
                    adress_clean = company_adress(adresse)
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'sitzadresse')
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'plz', _text = adress_clean.get('plz'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'strasse', _text = adress_clean.get('strasse'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'ort', _text = adress_clean.get('ort'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'land', _text = adress_clean.get('land'))
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'volltext', _text = adresse, attrib = {"relevanz" : "99"})
                    
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'quote', _text = quote)
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'rechtsform', _text = rechtsform, attrib = {"relevanz" : "1"})
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'oenbidentnummer', _text = oenbidentnummer, attrib = {"relevanz" : "2"})
                       
                
               
                    # Einlage -> ???
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('sitzadresse'), 'einlage', _text = '???')
                    
                 
                     
                        
                        
                        
                
                if e_n == 'management' or e_n == 'kontrollorgane':
                    try:
                        ressorts_liste = re.findall('\\((.+?)\\)', et)[-1]
                    except:
                        ressorts_liste = ''
                    
                    if ressorts_liste.find(',') == None:
                        pass
                    
                    else:
                        ressorts_liste = re.split(',', ressorts_liste)
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'ressorts')
                    
                    if ressorts_liste == ['']:
                        pass
                    
                    else:
                        for r in ressorts_liste:
                            ind_r = ressorts_liste.index(r)
                            create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('ressorts'), 'ressort')
                            create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et].find('ressorts').findall('ressort')[ind_r], 'text', _text = r.strip())
                    
                    # funktion -> ??
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'funktion', _text = '???')
                    
                    try:
                        vertretung = re.findall('<small>(.+?)</small>', et)[-1]
                    except:
                        vertretung = ''
                    create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'vertretung', _text = vertretung)
            
            
            
            # if it's not a link, some information is missing by default
            else:
                
                # search for the name which should be before the '(Anteil: ...)' part
                try: 
                    fullname = et[:re.search('\\(Anteil', re.sub('<.+?>', '', et)).start()]
                except:
                    fullname = et
               
                # can be different for each person, search for last occurence of the pattern up to the point where the person is in the html
                rolle = re.sub('<.+?>', '', re.findall('<h3>(.+?)</h3>', people[:people.find(et)])[-1]).upper()
                
                try:
                    quote = re.findall('\\(Anteil: (.+?)\\)', et)[-1]
                except:
                    quote = ''
                    
                create_SubElement(firma.find(e_n), sub_tag)
                # no id in the plain text
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'id')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'relation', _text = rolle)
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'geschlecht')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'fbnum')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'name', _text = fullname)
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'funktion')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'quote', _text = quote)
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'sitzadresse')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'rechtsform')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'oenbidentnummer')
                create_SubElement(firma.find(e_n).findall(sub_tag)[ind_et], 'vertretung')
                
            # eingetragen -> ??
        if e_n == 'eigentuemer':
            firma.remove(firma.find(e_l))
        
        else:
            firma.find(e_n).text = ''
            
        logging.info('finished %s' %e_n)



#%%   
  
# beteiligungen
if firma.find('beteiligungen') is None:
    logging.info('found no beteiligungen...')

else:
    
    try:
        # either last entry or not
        beteiligungen = re.findall('<span>Beteiligungen.+?<th>', str(soup.find('table', attrs={'id': 'firmenprofil-organe-beteiligungen'})))[0]
    except:
        beteiligungen = re.findall('<span>Beteiligungen.+?<td>.+?</table>', str(soup.find('table', attrs={'id': 'firmenprofil-organe-beteiligungen'})))[0]
    
    #beteiligungen = re.findall('<span>Beteiligungen.+?</table>', str(soup))[0]
 
    # in the form of <a href="/WirtschaftsCompass/profile/naturalperson/company/3990207">Futter Heide-Lene</a>,geb. 26.08.1940</span>
    beteiligungen_liste = re.findall('<a href=.+?</li>', beteiligungen)
    for bt in beteiligungen_liste:
        ind_bt = beteiligungen_liste.index(bt)
        
        fullname = re.sub('<.+?>', '', re.split('</a>', bt)[0])
        #fullname = re.split(',', re.sub('<.+?>', '', et))[0]
        
        # can be different for each person, search for last occurence of the pattern up to the point where the person is in the html
        rolle = re.sub('<.+?>', '', re.findall('<h3>als(.+?)</h3>', beteiligungen[:beteiligungen.find(bt)])[-1]).upper()
        
        
        link = re.findall('<a href="/WirtschaftsCompass/profile/(.+?)"', bt)[0]
        
        with io.open(temp.name + '\\' + re.sub('/', '_', link) + '.txt', 'r', encoding = 'utf-8') as file:
            parsed = file.read()
            try:
                adresse = re.findall('Adresse.*?<td>(.+?)</td>', parsed)[0]
            except:
                adresse = re.findall('Postanschrift.*?<td>(.+?)</td>', parsed)[0]
        
        
        # case 1:
        #</tr><tr><th><span>Adresse</span></th><td>Salurnerstraße 57<br/>6330 Kufstein, Tirol</td></tr><tr><th><span>Postanschrift</span>
        
        # case 2:
        #<tr><th>Adresse</th><td><span>Unterer Stadtplatz 24, 6330 Kufstein, Tirol, Österreich</span></td></tr>
        
            adresse = re.sub('<.+?>', '', adresse)
            
            if np.isin(element = np.array(re.split(' ', adresse)), test_elements = bundeslaender).any():
                fbnum = re.findall('Firmenbuchnummer.*?<td>(.+?)</td>', parsed)[0]
                rechtsform = re.findall('Rechtsform.*?<td>(.+?)</td>', parsed)[0]
                oenbidentnummer = re.sub('<.+?>', '', re.findall('OeNB Identnummer.*?<td>(.+?)</td>', parsed)[0])
            else:
                fbnum = ''
                rechtsform = ''
                oenbidentnummer = ''
        
        # quote appears only if the company is Gesellschafter? -> other structures with quotes?
        if np.isin(element = rolle, test_elements = ['GESELLSCHAFTER', 'SHAREHOLDER', 'AKTIONÄR']).any():
            quote = re.findall('\\(Anteil: (.+?)\\)', bt)[-1]

        else:
            quote = ''

        create_SubElement(firma.find('beteiligungen'), 'beteiligung')
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'id', _text = re.split('/', link)[-1])
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'relation', _text = rolle)
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'fbnum', _text = fbnum)

        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'fullname', _text = fullname, attrib = {"relevanz" : "99"})
                
        adress_clean = company_adress(adresse)
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'sitzadresse')
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt].find('sitzadresse'), 'plz', _text = adress_clean.get('plz'))
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt].find('sitzadresse'), 'strasse', _text = adress_clean.get('strasse'))
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt].find('sitzadresse'), 'ort', _text = adress_clean.get('ort'))
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt].find('sitzadresse'), 'bundesland', _text = adress_clean.get('bundesland'), attrib = {"art" : adress_clean.get('art')})
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt].find('sitzadresse'), 'land', _text = adress_clean.get('land'))
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt].find('sitzadresse'), 'volltext', _text = adresse, attrib = {"relevanz" : "99"})
        
        # Einlage -> ???
        #create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'einlage', _text = '???')
        
        # funktion -> ??
        #create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'funktion', _text = '???')
        
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'quote', _text = quote)
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'rechtsform', _text = rechtsform, attrib = {"relevanz" : "1"})
        create_SubElement(firma.find('beteiligungen').findall('beteiligung')[ind_bt], 'oenbidentnummer', _text = oenbidentnummer, attrib = {"relevanz" : "2"})
        
        
    firma.find('beteiligungen').text = '' 
    logging.info('finished beteiligungen')

# Konzernbaum
if firma.find('konzernbaum') is None:
    pass
else:
    firma.remove(firma.find('konzernbaum'))

#%%
####################################################################################################################################
# extract table 'firmenprofil-rechtstatsachen'
# -> not in every entry

try:
    firmenprofil_rechtsstatssachen = pd.read_html(str(soup.find('table', attrs={'id': 'firmenprofil-rechtstatsachen'})))[0]
except:
    firmenprofil_rechtsstatssachen = pd.DataFrame()

if firmenprofil_rechtsstatssachen.empty:
    logging.info('found no rechtsstatssachen...')

else:
     # either last row in the table or not
    try:
        rechtstatsachen = re.findall('<span>Rechtstatsachen.+</tr>', str(soup.find('table', attrs={'id': 'firmenprofil-rechtstatsachen'})))[0]
    except:
        rechtstatsachen = re.findall('<span>Rechtstatsachen.+</tbody>', str(soup.find('table', attrs={'id': 'firmenprofil-rechtstatsachen'})))[0]
    
    #rechtstatsachen = re.findall('<span>Rechtstatsachen.+?</tbody>', str(soup))[0]
    
    # split to get the different entries in a list
    rechtstatsachen_list = re.split('<li>', rechtstatsachen)[1:]
    
    create_SubElement(firma, 'rechtstatsachen', attrib = {"relevanz" : "1"})
    
    # eg. <h3>014</h3><span>Generalversammlungsbeschluss vom 10.09.2012 Neufassung des Gesellschaftsvertrages.</span></li>
    for rs in rechtstatsachen_list:
        ind_rs = rechtstatsachen_list.index(rs)
        create_SubElement(firma.find('rechtstatsachen'), 'rechtstatsache')
        
        rechtsnr = re.findall('<h3>(.+?)</h3>', rs)[0]
        text = re.findall('<span>(.+?)</span>', rs)[0]
        text = re.sub('<.+?>', '', text).strip()
        text = re.sub('  ', '', text)
        create_SubElement(firma.find('rechtstatsachen').findall('rechtstatsache')[ind_rs], 'rechtsnr', _text = rechtsnr)
        create_SubElement(firma.find('rechtstatsachen').findall('rechtstatsache')[ind_rs], 'text', _text = text)
    logging.info('finished rechtsstatssachen')  

#%%
#########################################################################################################################

# finalize tree structure -> parse tree into 'outer' tree which contains meta-information
final_tree = ET.ElementTree()
parser_export = ET.Element('parser_export')
final_tree._setroot(parser_export)
create_SubElement(parser_export, 'head')
create_SubElement(parser_export.find('head'), 'generator', _text = "Custom UCBA Firmenbuch Parser")
create_SubElement(parser_export.find('head'), 'produktname', _text = "compassXMLfirmenreport")
create_SubElement(parser_export.find('head'), 'version', _text = "Standard", attrib = {"ausgabeformat" : "1"})
create_SubElement(parser_export.find('head'), 'date', _text = datetime.datetime.now().strftime("%Y-%m-%d"))
create_SubElement(parser_export, 'nachricht')
create_SubElement(parser_export, 'body')

# attach created tree from firmenbuch to this structure
final_tree.find('body').append(ET.fromstring((ET.tostring(tree, encoding = 'UTF-8'))))



#%%
#from xml.dom import minidom
#print(minidom.parseString(ET.tostring(tree, encoding = 'UTF-8')).toprettyxml())
name = re.sub('"', '', re.sub(' ', '_', firma.find('name').text))

# export tree as xml file
final_tree.write('output/' + re.split('/', url)[-1] + '_' + name + '_' + '.xml', pretty_print = True, encoding = 'UTF-8')

logging.info('finished firmenprofil of company %s' %name)

if firma.find('gewerbedaten').find('id') is None:
    logging.info('found no gewerbe-id')
    logging.info('\n')

else:
    gewerbe_id = firma.find('gewerbedaten').find('id').text
    logging.info("extract gewerbeprofil")
    # execute script gewerbeprofil_to_xml which was made for extracting the gewerbeprofil of a company
    
    url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/" + gewerbe_id
    
    os.system("python gewerbeprofil_to_xml.py " + url)

# close google chrome
#os.system("taskkill /im chrome.exe /f")

# delete temporary directory
temp.cleanup()

# delete variables in order to make sure that there is no unwanted reuse between companies
for name in dir():
    if not name.startswith('_'):
        del globals()[name]
