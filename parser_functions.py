# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 09:30:45 2021

@author: N945017
"""
import requests
import lxml.etree as ET # for building xml elements
import numpy as np
import pandas as pd
import re
import datetime
import webbrowser
import time


def table_to_xml(row):
    # reformats html tables to a string reflecting xml format
    xml = []
    
    # special characters often lead to errors, paste them here
    # this comes from the fact that xml tags should only contain lowercase letters and _
    tag = re.sub('[^a-zA-Z0-9äüöß]', '_', row.values[0].lower())
    #tag = re.sub('[- ()\s]', '_', row.values[0].lower())
    
    xml.append('<' + tag +'>' + str(row.values[1]) + '</' + tag + '>')

    return '\n'.join(xml)


def create_SubElement(_parent, _tag, attrib = {}, _text = None, nsmap = None, **_extra):
    # function that will be used to add subelements to our xml tree
    # the function takes pararameters which are then set as the attributes for the xml Element
    # see https://stackoverflow.com/questions/33386943/python-lxml-subelement-with-text-value
    result = ET.SubElement(_parent, _tag, attrib, nsmap, **_extra)
    result.text = _text
    return result


def company_adress(_string):
    # take a string like 
    #       'Schönbrunner Straße 231<br/>1120 Wien' or
    #       'Schulstraße 22, 2871 Zöbern, Niederösterreich, Österreich' or
    #       'Boston, USA' or
    #       'Weissenbachl, Holzschlag 1277435 Unterkohlstätten, Burgenland    
    # and turn into adresselements (PLZ, Strasse, Ort, Bundesland, Land)
    
    # define list of bundeslaender
    #bundeslaender = ['Österreich', 'Burgenland', 'Niederösterreich', 'Wien', 'Oberösterreich', 'Steiermark', 'Kärnten', 'Tirol', 'Vorarlberg', 'Salzburg']
    # define Bundesländer, will be used later, orderd so that the attribute 'art' can be matched to the index position of the bundesland in this list
    bundeslaender = ['Österreich', 'Wien', 'Niederösterreich', 'Oberösterreich', 'Steiermark', 'Salzburg', 'Kärnten', 'Tirol', 'Vorarlberg', 'Burgenland']
    
    adress = _string
    
    # delete everything inbetween tags, which may unnecessarily be pasted as part of the adress
    adress = re.sub('<.+?>', '', adress)
    
    # split the adress into elements by space
    adress_elements = []
    for i in re.split(' ', ''.join(re.split(',', adress))):
        # see if elements can be further split -> addresses like xyzstreet 7/11
        if len(re.split('-', i)) >= 2:
            adress_elements.extend(re.split('-',i))
        elif len(re.split('/', i)) >= 2:
            adress_elements.extend(re.split('/', i))
        # for cases like 'Hietzinger Kai 131a1130 Wien' -> split by a
        elif len(re.split('[0-9][a-zA-Z][0-9]', i)) == 2:
            adress_elements.extend(re.split('([a-zA-Z])', i))
        else:
            adress_elements.append(i)
    
    # see if an element is a digit (PLZ, housenumber)
    to_int =[]      
                
    for i in adress_elements:
        if i.isdigit():
            to_int.append(int(i))
        else:
            to_int.append(i)
    
    # search for last numeric entry for knowing where the town starts (always after the PLZ)
    is_digit = [isinstance(x, int) for x in to_int]
    
    art = ''
    
    # if the adress only looks like 'New York, USA'
    if sum(is_digit) == 0:
        return({'plz': '',
           'ort': ' '.join(adress_elements[0:-1]),
           'strasse': '',
           'bundesland': '',
           'land': adress_elements[-1],
           'art': art})
    
    else:
        ind = np.where(is_digit)[0][-1]
    
    # the plz corresponds to the last 4 digits in the string
    plz = adress[ : re.search('.+[0-9]', adress).end()][-4:]
    
    # two different types of adresses -> with country or without country
    # with country -> additional ,country at the end:
    ws_split = re.split(' ', adress)
    
    #check if there are 2 or more commas and if the commas occur after city and bundesland and not in the street
    if len(re.findall(',', adress)) >= 2 and (np.isin([ws_split.index(i) for i in ws_split if ',' in i][-2:], [ws_split.index(x) for x in ws_split[-3:-1]]).all()):
        strasse = adress[ : re.search('.+[0-9]', adress).end() + 1][:-7]
        land = adress_elements[-1]
        bundesland = adress_elements[-2]
        if bundesland == 'Wien':
            ort = 'Wien'
        
        else:
            ort = ' '.join(adress_elements[ind + 1 : -2])
    
    else:
        strasse = adress[ : re.search('.+[0-9]', adress).end() + 1][:-5]
            
        if re.split(' ', adress)[-1] == 'Wien':
            ort = 'Wien'
            bundesland = 'Wien'
            land = 'Österreich'
        
        else:
            ort = ' '.join(map(str, to_int[ind + 1: -1]))
            bundesland = adress_elements[-1]
            if np.isin(bundesland, bundeslaender):
                land = 'Österreich'
            else:
                land = adress_elements[-1]
    
    if np.isin(bundesland, bundeslaender):
        art = str(bundeslaender.index(bundesland))   
        
    else:
        art = ''
       
    res = {'plz': plz,
           'ort': ort,
           'strasse': strasse,
           'bundesland': bundesland,
           'land': land,
           'art': art}
    
    return(res)

#print(company_adress("Wollzeile 11/2. OG1010 Wien"))
#print(company_adress("Boston, USA"))
#print(company_adress("Schulstraße 22, 2871 Zöbern, Niederösterreich, Österreich"))
#print(company_adress("Weissenbachl, Holzschlag 1277435 Unterkohlstätten, Burgenland"))
#print(company_adress('Staufenstraße 1/S/35161 Elixhausen, Salzburg'))

#print(company_adress("9 E Pelican Street34113 Naples, Florida, USA, USA"))
#print(company_adress("1 N.372 Main Street, Glen Ellyn Illinois 60137-3576 USA, USA"))
#print(company_adress("11-13, Boulevard de la Foire 1528 Luxemburg, LUX"))


def name_split(_string):
    fullname = _string
    
    # filter out titles (always after comma)
    if len(re.split(',', fullname)) > 1:
        titel = fullname[fullname.find(',') + 1:].lstrip()
        name_elements = re.split(' ', ''.join(re.split(',', fullname)[:-1]))
    else:
        titel = ''
        name_elements = re.split(' ', fullname)
    #print('\n')
    #print(fullname)   
    #print(name_elements)
    for ne in name_elements:
        # if a name with a '-' is not the last element, then all elements after that are vornamen
        if ne.find('-') != -1 and name_elements.index(ne) + 1 != len(name_elements):
            vorname = ' '.join(name_elements[name_elements.index(ne) + 1:])
            nachname = ' '.join(name_elements[:name_elements.index(ne) + 1])
            break
        # if a name with a '-' is the last elements, then all before that are nachnamen
        if ne.find('-') != -1 and name_elements.index(ne) + 1 == len(name_elements):
            vorname = ' '.join(name_elements[name_elements.index(ne):])
            nachname = ' '.join(name_elements[:name_elements.index(ne)])
            break
        # for names with spanish 'de'
        if ne == 'de':
            vorname = ' '.join(name_elements[name_elements.index(ne) + 2:])
            nachname = ' '.join(name_elements[name_elements.index(ne) : name_elements.index(ne) + 2])
            break
        else:     
            vorname = ' '.join(name_elements[1:])
            nachname = ' '.join(name_elements[:1])
    #print(name_elements)
    
    #res = titel
    
    res = {'vorname': vorname,
           'nachname': nachname,
           'titel': titel}
    
    return(res)

#fullname = 'Gasselsberger Franz, Konsul KR Dr. MBA'
#print(name_split('Hofstätter-Pobst Gregor, Mag.'))
#print(name_split('Gasselsberger Franz, Konsul KR Dr. MBA'))
#print(name_split('Anderle Wilhelm-Christian, Mag. (FH)'))
#print(name_split('Aliberti Amidani Livia, Dr.'))
#print(name_split('Morales Albinana-Rosner Marion, Mag.'))
#print(name_split('Plater Alejandro Douglass'))
#print(name_split('Nettesheim Heinz Norbert'))
#print(name_split('de Marchis Ranieri, MBA'))


def establish_connection():
    s = requests.Session()

    s.proxies = {
     "http": "http://proxy.mib-is.org:8080",
     "https": "http://proxy.mib-is.org:8080",
                }
    
    try:
        s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
    
    except:
        for i in range(5):
            try:
                s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
                if i != 0:
                    print('sucess!')
                break
            except:
                print('lost the connection, open the browser and try again...')
                webbrowser.open('https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass', autoraise = False)
                time.sleep(6)
                s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")                
            if i == 5:
                print("tried 5 times, didn't work...")
                
                
def gewerbe_adress(_string):
    # take a string like 
    #       '1120 Wien, Schönbrunner Straße 231, (Hauptsitz)'   
    # and turn into adresselements (PLZ, Strasse, Ort, Bundesland, Land)
    
    # define list of bundeslaender
    bundeslaender = ['Österreich', 'Burgenland', 'Niederösterreich', 'Wien', 'Oberösterreich', 'Steiermark', 'Kärnten', 'Tirol', 'Vorarlberg', 'Salzburg']
    
    adress = _string
    # delete everything inbetween tags, which may unnecessarily be pasted as part of the adress
    adress = re.sub('<.+?>', '', adress)
    
    # split the adress into elements by space
    adress_elements = []
    for i in re.split(' ', ''.join(re.split(',', adress))):
        # see if elements can be further split -> addresses like xyzstreet 7/11
        if len(re.split('-', i)) >= 2:
            adress_elements.extend(re.split('-',i))
        elif len(re.split('/', i)) >= 2:
            adress_elements.extend(re.split('/', i))
        # for cases like 'Hietzinger Kai 131a1130 Wien' -> split by a
        elif len(re.split('[0-9][a-zA-Z][0-9]', i)) == 2:
            adress_elements.extend(re.split('([a-zA-Z])', i))
        else:
            adress_elements.append(i)
    #print(adress_elements)    
    
    # see if an element is a digit (PLZ, housenumber)
    # to_int =[]      
                
    # for i in adress_elements:
    #     if i.isdigit():
    #         to_int.append(int(i))
    #     else:
    #         to_int.append(i)
    
    # # search for first numeric entry for knowing where the town starts (always after the PLZ)
    # is_digit = [isinstance(x, int) for x in to_int]
    
    # # if the adress only looks like 'New York, USA'
    # if sum(is_digit) == 0:
    #     return({'plz': '',
    #         'ort': ' '.join(adress_elements[0:-1]),
    #         'strasse': '',
    #         'bundesland': '',
    #         'land': adress_elements[-1]})
    
    # else:
    #     ind = np.where(is_digit)[0][0]
    # #print(to_int)
    # #print(ind)
    
    # the plz corresponds to the first 4 digits in the string
    plz = adress_elements[0]
    #plz = adress[ : re.search('.+[0-9]', adress).end()][-4:]
    
    # type of niederlassung, eg. Filiale, Hauptsitz
    type_ = re.sub('\\(|\\)', '', adress_elements[-1])
    
    strasse = re.split(',', adress)[-2].lstrip()
    
    # some town names have commas in them, start extraction at the end of the plz and end it at the beginning of the street
    start = adress.find(plz) + len(plz)
    end = adress.find(strasse) - 2
    ort = adress[start : end].lstrip()
    
    # # two different types of adresses -> with country or without country
    # # with country -> additional ,country at the end:
    # ws_split = re.split(' ', adress)
    
    # #check if there are 2 or more commas and if the commas occur after city and bundesland and not in the street
    # if len(re.findall(',', adress)) >= 2 and (np.isin([ws_split.index(i) for i in ws_split if ',' in i][-2:], [ws_split.index(x) for x in ws_split[-3:-1]]).all()):
    #     strasse = adress[ : re.search('.+[0-9]', adress).end() + 1][:-7]
    #     land = adress_elements[-1]
    #     bundesland = adress_elements[-2]
    #     if bundesland == 'Wien':
    #         ort = 'Wien'
        
    #     else:
    #         ort = ' '.join(adress_elements[ind + 1 : -2])
    
    # else:
    #     strasse = adress[ : re.search('.+[0-9]', adress).end() + 1][:-5]
            
    #     if re.split(' ', adress)[-1] == 'Wien':
    #         ort = 'Wien'
    #         bundesland = 'Wien'
    #         land = 'Österreich'
        
    #     else:
    #         ort = ' '.join(map(str, to_int[ind + 1: -1]))
    #         bundesland = adress_elements[-1]
    #         if np.isin(bundesland, bundeslaender):
    #             land = 'Österreich'
    #         else:
    #             land = adress_elements[-1]
          
    res = {'plz': plz,
           'ort': ort,
           'strasse': strasse,
           'type': type_}
    return(res)