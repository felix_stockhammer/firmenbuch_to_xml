# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 09:18:59 2021

@author: N945017
"""
# Gewerbeprofil

import requests
from bs4 import BeautifulSoup as bs
import lxml.etree as ET # for building xml elements
from xml.dom import minidom # for printing the xml structure
import numpy as np
import pandas as pd
import re
import datetime
import logging
import webbrowser
import time
from parser_functions import *
import os
import sys
#%%
# set up the log file
logging.basicConfig(filename = 'parser.log', level = logging.DEBUG)

#%%
s = requests.Session()

s.proxies = {
 "http": "http://proxy.mib-is.org:8080",
 "https": "http://proxy.mib-is.org:8080",
}

try:
    #print('try to establish a connection...')
    s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
    #print('sucess!')

except:
    for i in range(5):
        try:
            #print('trying again...')
            s.get("https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass")
            #print('sucess!')
            break
        except:
            #print('open the browser and try again...')
            webbrowser.open('https://daten.compass.at/sso/login/process?userDomain=OB6ZT6&autoLogin=true&target=/WirtschaftsCompass', autoraise = False)
            time.sleep(6)
        #if i == 5:
            #print("tried 5 times, didn't work...")

logging.info("Current time: %s" %datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
logging.info("---Gewerbeprofil---")

# Compass
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/1270337"

# Porr
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/879838"

# Bank Austria
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/1294469"

# Lumos
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/73967819"

# uniforce
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/1324068"

# truck-center-süd
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/17297711"

# gelöschtes gewerbe
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/2007740"

#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/9571053"

#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/9549839"

#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/3723674"
#url = "https://daten.compass.at/WirtschaftsCompass/profile/tradeperson/888004"
url = sys.argv[1]

r = s.get(url)
 
   
#print(f'Request status Code: {r.status_code}')
logging.info('Retrieved webpage with id %s' %re.split('/', url)[-1])


html_messy = r.text
html = html_messy.translate(str.maketrans({'\n': None, '\t': None, '\r': None}))

soup = bs(html, 'html.parser')

main_content = re.findall('<section class="detail-content">.+?</section>', str(soup))[0]

main_content_table = pd.read_html(main_content)[0]

main_content_table.rename(columns = {main_content_table.columns[0]: 'Name', main_content_table.columns[1]: 'Value'}, inplace = True)

# apply function to our table and turn into string
main_content_string = '\n'.join(main_content_table.apply(table_to_xml, axis=1))
# add 'root' node for xml
main_content_string = '<firma>\n' + main_content_string + '\n</firma>'
# the & sign leads to errors, so replace it
main_content_string = re.sub('&', '&amp;', main_content_string)

# create xml
tree = ET.ElementTree(ET.fromstring(re.sub('\n', '', main_content_string)))

firma = tree.getroot()
logging.info("created basic tree")

# rename elements which are captured correctly but with the wrong tag name
for element in firma:
    if element.tag == 'global_location_number__gln_':
        element.tag = 'gln'
        element.set("relevanz", "3")
    if element.tag == 'firmenbuchnummer':
        element.tag = 'fbnum'
        element.set("relevanz", "3")
        
#firma.find('gln').set("relevanz", "3")
        
create_SubElement(firma, 'name', _text = re.findall('<title>(.+?)\\(Gewerbe\\)', str(soup))[0].rstrip(), attrib = {"relevanz" : "3"})

if firma.find('standorte') is None:
    pass

else:
    firma.remove(firma.find('standorte'))


# status gewerbe
header = re.findall('<section class="detail-header">(.+?)</section>', str(soup))[0]
if header.find('Gewerbe ruhend</span>') != -1:
    status = 'ruhend'
elif header.find('Gewerbe gelöscht</span>') != -1:
    status = 'geloescht'

# according to gewerbereport.xsd there's the category 'Scheinunternehmen'
elif header.find('Scheinunternehmen') != -1:
    status = 'Scheinunternehmen'
else:
    status = 'aufrecht'
    
create_SubElement(firma, 'status', _text = status, attrib = {"relevanz" : "3"})
create_SubElement(firma, 'id', _text = re.split('[/]', url)[-1], attrib = {"relevanz" : "3"})
create_SubElement(firma, 'link', _text = url, attrib = {"relevanz" : "99"}) 

# selbstgewählte unternehmensbezeichnung -> How does it look in the html?? Here the name is used again
create_SubElement(firma, 'selbstgewaehlte_unternehmensbezeichnung')
create_SubElement(firma.find('selbstgewaehlte_unternehmensbezeichnung'), 'selbstgewaehlte_unternehmensbezeichnung', _text = firma.find('name').text, attrib = {"relevanz" : "3"})


sitze = re.findall('<section class="detail-block divider" id="section_address_0">.+</section>', re.sub('>\s<', '><', str(soup)))[0]

#sitze_list = re.split('</section>\s*?</div>\s*?</section>', sitze)
sitze_list = re.split('</section></div></section>', sitze)

create_SubElement(firma, 'standorte')

# initialize list with berechtigungen, gets filled with data from each sitz
berechtigungswortlaute = []

logging.info("found %s standorte" %len(sitze_list[:-1]))

for si in sitze_list[:-1]:
     
    
    create_SubElement(firma.find('standorte'), 'standort')
    ind_si = sitze_list.index(si)
    # kontakt
    adresse = re.findall('Standort (.+?)</h3>', si)[0]
    # adresse in another form than for firmen
    # eg. '1120 Wien, Schönbrunner Straße 231, (Hauptsitz)
    adress_clean = gewerbe_adress(adresse)
      
    create_SubElement(firma.find('standorte').findall('standort')[ind_si], 'adresse', attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('adresse'), 'volltext', _text = adresse, attrib = {"relevanz" : "99"})
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('adresse'), 'plz', _text = adress_clean.get('plz'), attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('adresse'), 'ort', _text = adress_clean.get('ort'), attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('adresse'), 'strasse', _text = adress_clean.get('strasse'), attrib = {"relevanz" : "3"})
    
    create_SubElement(firma.find('standorte').findall('standort')[ind_si], 'haupt', _text = adress_clean.get('type'), attrib = {"relevanz" : "3"})
    
    # status standort
    if si.find('Gewerbe ruhend</span></header>') != -1:
        status_st = 'ruhend'
    elif si.find('Gewerbe gelöscht</span></header>') != -1:
        status_st = 'geloescht'
    else:
        status_st = 'aufrecht'
        
    create_SubElement(firma.find('standorte').findall('standort')[ind_si], 'status_standort', _text = status_st, attrib = {"relevanz" : "99"})
    
    create_SubElement(firma.find('standorte').findall('standort')[ind_si], 'kontakt', attrib = {"relevanz" : "3"})
    
    if si.find('<th>Telefon') == -1:
        telefon = ''
    else:
        telefon = re.sub('<.+?>', '', re.findall('<th>Telefon(.+?)</tr>', si)[0]).lstrip().rstrip()
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt'), 'telefonnummern')
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt').find('telefonnummern'), 'telefon', attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt').find('telefonnummern').find('telefon'), 'nummer', _text = telefon)
        
    
    if si.find('<th>Fax') == -1:
        fax = ''
    else:
        fax = re.sub('<.+?>', '', re.findall('<th>Fax(.+?)</tr>', si)[0]).lstrip().rstrip()
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt'), 'faxnummern')
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt').find('faxnummern'), 'fax', attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt').find('faxnummern').find('fax'), 'nummer', _text = telefon)
    
    if si.find('<th>Internet-Adresse') == -1:
        internet = ''
    else:
        internet = re.sub('<.+?>', '', re.findall('<th>Internet-Adresse(.+?)</tr>', si)[0]).lstrip().rstrip()
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt'), 'homepages')    
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt').find('homepages'), 'www', _text = internet)
   
    if si.find('<th>E-Mail') == -1:
        email = ''
    else:
        email = re.sub('<.+?>', '', re.findall('<th>E-Mail(.+?)</tr>', si)[0]).lstrip().rstrip()
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt'), 'emailadressen')
    create_SubElement(firma.find('standorte').findall('standort')[ind_si].find('kontakt').find('emailadressen'), 'email', _text = email)

    # gewerbewortlaute
    # extract gewerbewortlaute and append to list, which will be used later
    
    #elif re.split('</table></section>', si) == []:
    if re.split('</table></section>', si)[1:] == []:
        # if there's only one berechtigungswortlaut for the sitz
        berechtigungswortlaute.extend(re.split('</table></section>', si))
    elif len(sitze_list) <=2:
        # if there's only one sitz...
        #berechtigungswortlaute.append(si)
        #berechtigungswortlaute.append(re.split('</table></section>', si)[-1])
        berechtigungswortlaute.extend(re.split('</table></section>', si))
    else:
        berechtigungswortlaute.extend(re.split('</table></section>', si))
        #berechtigungswortlaute = re.split('</table></section>', si)

#%%        
# delete unnecessary entries from berechtigungsworlaute -> entries which provide information about the sitz but not
#  about the berechtigungswortlaut
berechtigungswortlaute_new = [i for i in berechtigungswortlaute if i.find('Berechtigungswortlaut') != -1]


#print(len(berechtigungswortlaute))
create_SubElement(firma, 'berechtigungen', attrib = {"relevanz" : "3"})

# initialize counter for indexing positions of details of berechtigungen
count_berechtigung = 0

# initialize list of gewerberechtliche geschäftsführer
gf_list = []

# initialize list of inhaber
inhaber_list = []

for wortlaut in berechtigungswortlaute_new:
   
    be = wortlaut
        
    create_SubElement(firma.find('berechtigungen'), 'berechtigung', attrib = {"relevanz" : "3"})
    #ind_be = berechtigungswortlaute.index(wortlaut)
    
    wl = re.sub('<.+?>|:', '', re.findall('<span>Berechtigungswortlaut(.+?)</h3>', be)[0]).lstrip()
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'wortlaut', _text = wl, attrib = {"relevanz" : "99"})    
    #print(wortlaut)
    
    be_table = pd.read_html(be)[0]
    be_table.rename(columns = {be_table.columns[0]: 'Name', be_table.columns[1]: 'Value'}, inplace = True)
    #print(be_table)
    if be_table.loc[be_table.Name == 'GISA-Zahl'].empty:
        gisa_zahl = ''
    else:
        gisa_zahl = be_table.loc[be_table.Name == "GISA-Zahl"].Value.values[0]
    
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'gisa_zahl', _text = gisa_zahl, attrib = {"relevanz" : "3"}) 
    #print(gisa_zahl)
    
    # status berechtigungswortlaut
    if be.find('Gewerbe ruhend') != -1:
        status_be = 'ruhend'
    elif be.find('Gewerbe gelöscht') != -1:
        status_be = 'geloescht'
    else:
        status_be = 'aufrecht'
    
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'status_berechtigungswortlaut', _text = status_be, attrib = {"relevanz" : "3"}) 
    
    # ruhend
    if status_be == 'ruhend':
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'ruhend', _text = '1', attrib = {"relevanz" : "3"}) 
    else:
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'ruhend', _text = '0', attrib = {"relevanz" : "3"})
    
    # datum, not specified what this is about, instead I pasted eintragedatum and enddatum of the berechtigungswortlaut
    if status_be == 'aufrecht':
        eintragedatum = be_table.loc[be_table.Name == 'Eingetragen am'].Value.values[0]
        enddatum = ''
    else:
        eingetragen_von_bis = be_table.loc[be_table.Name == 'Eingetragen von / bis'].Value.values[0]
        eintragedatum = re.findall('von: (.+?)bis', eingetragen_von_bis)[0]
        enddatum = re.findall('bis: (.+)', eingetragen_von_bis)[0]
    # print(eintragedatum)
    # print(enddatum)
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'eintragedatum', _text = eintragedatum, attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'enddatum', _text = enddatum, attrib = {"relevanz" : "3"}) 
    
    # pacht -> where to find??
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'pacht', _text = "???", attrib = {"relevanz" : "3"})

    if be_table.loc[be_table.Name == 'Kammer'].empty:
        kammer = ''
    else:
        kammer = be_table.loc[be_table.Name == 'Kammer'].Value.values[0]
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'kammer_bundesland', _text = kammer, attrib = {"relevanz" : "3"}) 
        
    
    if be_table.loc[be_table.Name == 'Spartenname'].empty:
        spartenname = ''
    else:
        spartenname = be_table.loc[be_table.Name == 'Spartenname'].Value.values[0]
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'spartenname', _text = spartenname, attrib = {"relevanz" : "3"}) 
    
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'wkoklassifizierung', attrib = {"relevanz" : "3"})
    
    if be_table.loc[be_table.Name == 'Berufszweig'].empty:
        berufszweig = ''
    else:
        berufszweig = str(be_table.loc[be_table.Name == 'Berufszweig'].Value.values[0])
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung'), 'berufszweig', _text = berufszweig, attrib = {"relevanz" : "3"})         
    
    if be_table.loc[be_table.Name == 'Fachgruppe'].empty:
        fachgruppe = ''
    else:
        fachgruppe = be_table.loc[be_table.Name == 'Fachgruppe'].Value.values[0]
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung'), 'fachgruppe', _text = fachgruppe, attrib = {"relevanz" : "3"}) 
    
    
    if be_table.loc[be_table.Name == 'OENACE 2008'].empty:
        oenace = ''
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung'), 'oenace', attrib = {"relevanz" : "2"}) 
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace'), 'klassifizierung')  
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace').find('klassifizierung'), 'wert',  _text = oenace)
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace').find('klassifizierung'), 'text',  _text = oenace)
    
    else:
        oenace = be_table.loc[be_table.Name == 'OENACE 2008'].Value.values[0]
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung'), 'oenace', attrib = {"relevanz" : "2"}) 
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace'), 'klassifizierung')  
        #create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace').find('klassifizierung'), 'wert',  _text = re.findall('[0-9]+\\.[0-9]+-[0-9]', oenace)[0].rstrip())
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace').find('klassifizierung'), 'wert',  _text = re.split(" ", oenace)[0])
        create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung].find('wkoklassifizierung').find('oenace').find('klassifizierung'), 'text',  _text = re.findall('\\((.+)\\)', oenace)[0].rstrip())
    
    # behoerde can be found in html but not in compass xml, paste anyway
    if be_table.loc[be_table.Name == 'Behörde gem. ECG'].empty:
        behoerde = ''
    else:
        behoerde = be_table.loc[be_table.Name == 'Behörde gem. ECG'].Value.values[0]
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'behoerde', _text = behoerde, attrib = {"relevanz" : "99"}) 
    
    # id_person, id_adresse -> where to find??
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'id_person', _text = '???', attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('berechtigungen').findall('berechtigung')[count_berechtigung], 'id_adresse', _text = '???', attrib = {"relevanz" : "3"})
  
    
    # add to counter
    count_berechtigung += 1
    
    
    if be_table.loc[be_table.Name == 'Gewerberechtlicher Geschäftsführer'].empty:
        pass
    else:
        gewerbe_gf_list = re.findall('<th>Gewerberechtlicher Geschäftsführer(.+?)</td>', be)[0]
        gewerbe_gf_list = re.split('<li>', gewerbe_gf_list)
        gf_list.extend(gewerbe_gf_list)
        
    if be_table.loc[be_table.Name == 'Inhaber'].empty:
        pass
    else:
        inhaber_list_1 = re.findall('<th>Inhaber(.+?)</td>', be)[0]
        inhaber_list_1 = re.split('<li>', inhaber_list_1)
        inhaber_list.extend(inhaber_list_1)
 
logging.info("found %s berechtigungen" %count_berechtigung)    
 
# unique elements from inhaber_list
inhaber_list = list(dict.fromkeys(inhaber_list))
logging.info("found %s inhaber" %len(inhaber_list))

create_SubElement(firma, 'eigentuemer')

for inh in inhaber_list:
    ind_inh = inhaber_list.index(inh)   
    create_SubElement(firma.find('eigentuemer'), 'eigner', attrib = {"relevanz" : "3"})            
    
    fullname = re.split(',', re.sub('<.+?>', '', inh))[0]
    name = name_split(fullname)
    
    try:
        elements = re.split(' ', re.split(',', re.sub('<.+?>', '', inh))[-1])
        geburt = datetime.datetime.strptime(re.sub('[a-z]', '', elements[elements.index('geb.') + 1]), '%d.%m.%Y').strftime('%Y-%m-%d')
    except:
        geburt = ''  
        
    create_SubElement(firma.find('eigentuemer').findall('eigner')[ind_inh], 'relation', _text = 'Gewerbeinhaber')
    
    create_SubElement(firma.find('eigentuemer').findall('eigner')[ind_inh], 'geburt', _text = geburt, attrib = {"relevanz" : "3"})
    
    create_SubElement(firma.find('eigentuemer').findall('eigner')[ind_inh], 'fullname', _text = name.get('fullname'), attrib = {"relevanz" : "99"})
    create_SubElement(firma.find('eigentuemer').findall('eigner')[ind_inh], 'titel', _text = name.get('titel'), attrib = {"relevanz" : "99"})
    create_SubElement(firma.find('eigentuemer').findall('eigner')[ind_inh], 'vorname', _text = name.get('vorname'), attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('eigentuemer').findall('eigner')[ind_inh], 'nachname', _text = name.get('nachname'), attrib = {"relevanz" : "3"})
     
 
# unique elements from gf_list
gf_list = list(dict.fromkeys(gf_list)) 
logging.info("found %s geschaeftsfuehrer" %len(gf_list))   

create_SubElement(firma, 'management', attrib = {"relevanz" : "3"})           

# if there's no explicit gewerberechtlicher geschäftsführer and there's only one inhaber, then the inhaber is the gf 
if (len(gf_list) == 0) & (len(inhaber_list) == 1):
    create_SubElement(firma.find('management'), 'person', attrib = {"relevanz" : "3"})
    create_SubElement(firma.find('management').find('person'), 'relation', _text = "gewerblicher Geschäftsführer")   
    create_SubElement(firma.find('management').find('person'), 'fullname', _text = firma.find('eigentuemer').find('eigner').find('fullname').text, attrib = {"relevanz" : "99"}) 
    create_SubElement(firma.find('management').find('person'), 'titel', _text = firma.find('eigentuemer').find('eigner').find('titel').text, attrib = {"relevanz" : "99"}) 
    create_SubElement(firma.find('management').find('person'), 'vorname', _text = firma.find('eigentuemer').find('eigner').find('vorname').text, attrib = {"relevanz" : "3"}) 
    create_SubElement(firma.find('management').find('person'), 'nachname', _text = firma.find('eigentuemer').find('eigner').find('nachname').text, attrib = {"relevanz" : "3"}) 
    create_SubElement(firma.find('management').find('person'), 'geburt', _text = firma.find('eigentuemer').find('eigner').find('geburt').text, attrib = {"relevanz" : "3"}) 

else:

    for gf in gf_list:
        create_SubElement(firma.find('management'), 'person', attrib = {"relevanz" : "3"})
        ind_gf = gf_list.index(gf)
    
        gf = re.sub('<.+?>', '', gf)
        fullname = re.split(',', gf)[0]
        name = name_split(fullname)
        
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'relation', _text = "gewerblicher Geschäftsführer")
        
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'fullname', _text = fullname, attrib = {"relevanz" : "99"}) 
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'titel', _text = name.get('titel'), attrib = {"relevanz" : "99"}) 
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'vorname', _text = name.get('vorname'), attrib = {"relevanz" : "3"}) 
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'nachname', _text = name.get('nachname'), attrib = {"relevanz" : "3"}) 
                      
        
        elements = re.split(' ', re.split(',', re.sub('<.+?>', '', gf))[-1])
    
        if np.isin('geb.', elements).any():
            geburt = datetime.datetime.strptime(re.sub('[a-z]', '', elements[elements.index('geb.') + 1]), '%d.%m.%Y').strftime('%Y-%m-%d') 
        else:
            geburt = ''
        
        if np.isin('am:', elements).any():
            bestelldatum = datetime.datetime.strptime(re.sub('[a-z]', '', elements[elements.index('am:') + 1]), '%d.%m.%Y').strftime('%Y-%m-%d')  
        else:
            bestelldatum = ''
            
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'geburtsdatum', _text = geburt, attrib = {"relevanz" : "3"}) 
        create_SubElement(firma.find('management').findall('person')[ind_gf], 'bestelldatum', _text = bestelldatum) 



#%%
#########################################################################################################################

# finalize tree structure -> parse tree into 'outer' tree which contains meta-information
final_tree = ET.ElementTree()
parser_export = ET.Element('parser_export')
final_tree._setroot(parser_export)
create_SubElement(parser_export, 'head')
create_SubElement(parser_export.find('head'), 'generator', _text = "Custom UCBA Firmenbuch Parser")
create_SubElement(parser_export.find('head'), 'produktname', _text = "compassXMLgewerbereport")
create_SubElement(parser_export.find('head'), 'version', _text = "Standard", attrib = {"ausgabeformat" : "1"})
create_SubElement(parser_export.find('head'), 'date', _text = datetime.datetime.now().strftime("%Y-%m-%d"))
create_SubElement(parser_export, 'nachricht')
create_SubElement(parser_export, 'body')

# attach created tree from firmenbuch to this structure
final_tree.find('body').append(ET.fromstring((ET.tostring(tree))))

    
#%%
#print(minidom.parseString(ET.tostring(tree, encoding = 'UTF-8')).toprettyxml())
name = re.sub(' ', '_', re.findall('<title>(.+?)\\(Gewerbe\\)', str(soup))[0].rstrip())

logging.info('finished gewerbe %s' %name)
logging.info('\n')

final_tree.write('output/' + re.split('/', url)[-1] + '_' + name + '_gewerbe' + '.xml', pretty_print = True, encoding = 'UTF-8')

# close google chrome
#os.system("taskkill /im chrome.exe /f")

#delete variables in order to make sure that there is no unwanted reuse between companies
for name in dir():
    if not name.startswith('_'):
        del globals()[name]