# README #

##### Workflow with docker
```
# clone this repository  via git clone
# the user who clones this repository is also the owner of the resulting xml files

# change directory
cd firmenbuch_to_xml


# create input.csv
echo "9571053" > input.csv   # or move exiting input.csv to firmenbuch_to_xml/input.csv



# with the following command it will automatically build the docker image if it does not exist yet.
# Then it will also run firmenbuch_to_xml.py. If successful, the results can be found in the output folder with
# permissions set to user who cloned the repository
. run.sh
ls output/
```
#
### firmenbuch_to_xml.py ###

Code for extracting the 'firmenprofil' area of a company on daten.compass.at/WirtschaftsCompass.

### gewerbeprofil_to_xml.py ###

Code for extracting the 'gewerbeprofil' area of a company on daten.compass.at/WirtschaftsCompass.

### parser_functions.py ###

Includes custom functions for the parser. Run one time before starting the parser.

### parser.py ###

Script gets a list of compass-ids as input (in a csv) and then runs firmenbuch_to_xml and gewerbeprofil_to_xml to convert the pages to xml.
